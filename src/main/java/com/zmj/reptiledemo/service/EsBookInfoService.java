package com.zmj.reptiledemo.service;


import com.zmj.reptiledemo.repository.BookInfo;

import java.util.List;

public interface EsBookInfoService {

    void batchAddBookInfo(List<BookInfo> bookInfos);

    void addBookInfo(BookInfo Book,boolean exit);

    void deletedBookInfoById(String id);

    void updateBookInfo(BookInfo bookInfo);

    List<BookInfo> getByBookIdAndSort(String bookId,String  sort);

    int getById(String readUrl);

    List<BookInfo> getByBookId(String trimId);
}
