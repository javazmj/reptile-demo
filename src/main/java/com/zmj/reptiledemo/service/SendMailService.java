package com.zmj.reptiledemo.service;

import org.springframework.mail.SimpleMailMessage;

import javax.mail.MessagingException;

public interface SendMailService {

   void sendSimpleMail(SimpleMailMessage simpleMailMessage);

   void sendRegisterHtmlMail(String email) throws MessagingException;
}
