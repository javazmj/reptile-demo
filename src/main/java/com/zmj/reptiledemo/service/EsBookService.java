package com.zmj.reptiledemo.service;

import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.vo.ResultVO;

import java.util.List;

public interface EsBookService {

    void batchAddBook(List<Book> Books);

    void addBook(Book Book);

    void deletedBookById(String id);

    void updateBook(Book Book);

    List<Book> getQuery(String type,Object name);

    int getById(String bookId);

    List<Book> getRedisBookRandom(int code,String category,int n);

    List<Book> getRedisBookAll(int category);

    List<Book> queryAll();

    List<Book> querySearch(String search);

    ResultVO searchSite(String search);
}
