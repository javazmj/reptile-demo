package com.zmj.reptiledemo.service.impl;

import com.zmj.reptiledemo.service.SendMailService;
import com.zmj.reptiledemo.utils.RandomUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class SendMailServiceImpl implements SendMailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private StringRedisTemplate redisTemplate;


    @Override
    public void sendSimpleMail(SimpleMailMessage simpleMailMessage) {
        mailSender.send(simpleMailMessage);
    }

    @Override
    public void sendRegisterHtmlMail(String email) throws MessagingException {
        String verifCode = RandomUtils.generateNumber(6);
        //该验证码三十分钟有效
        redisTemplate.opsForValue().set(email,verifCode,30, TimeUnit.MINUTES);
        //创建一个SimpleMailMessage对象
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        //需要创建一个MimeMessageHelper对象，相关参数和简单邮件类似
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("536304123@qq.com");
        helper.setTo(email);
        helper.setSubject("欢迎注册zmj小说站,这是一封注册验证的邮件");

        StringBuffer sb = new StringBuffer();
        sb.append("<h1>zmj小说站</h1>")
            .append("<p style='color:#F00'>http://book.zmjmall.com</p>")
            .append("<p>注册验证码:  ")
            .append("<span style='text-align:left; color:#FF0000'>")
                .append(verifCode)
                .append("</span>  &nbsp;&nbsp;(有效时间为三十分钟)</p>");
        helper.setText(sb.toString(), true);

        mailSender.send(mimeMessage);

    }
}
