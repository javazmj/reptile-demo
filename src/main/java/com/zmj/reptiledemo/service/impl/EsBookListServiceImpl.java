package com.zmj.reptiledemo.service.impl;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.mapper.BookListRepository;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.service.EsBookListService;
import com.zmj.reptiledemo.utils.JsonUtil;
import com.zmj.reptiledemo.utils.PageHelper;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class EsBookListServiceImpl implements EsBookListService {

    @Autowired
    private BookListRepository bookListRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 批量新增
     **/
    @Override
    public void batchAddBookList(List<BookList> bookLists) {
        if(CollectionUtils.isEmpty(bookLists)) {
            return ;
        }
        List<IndexQuery> queries = Lists.newArrayListWithExpectedSize(bookLists.size());
        IndexQuery indexItem  = null;
        for(BookList bookList :bookLists) {
            indexItem = new IndexQuery();
            indexItem.setObject(bookList);
            queries.add(indexItem);
        }
//        bookListRepository.saveAll(bookLists);

        elasticsearchTemplate.bulkIndex(queries);
    }

    @Override
    public void addBookList(BookList bookList) {
        bookListRepository.save(bookList);
    }

    @Override
    public void deletedBookListById(String id) {
        bookListRepository.deleteById(id);
    }

    /**
     * 根据BookListId更新信息
     */
    @Override
    public void updateBookList(BookList bookList) {
        UpdateQuery updateQuery = new UpdateQuery();
        updateQuery.setId(bookList.getBookId());
        updateQuery.setClazz(BookList.class);
        bookList.setBookId(null);
        UpdateRequest request = new UpdateRequest();
        request.doc(JsonUtil.toJson(bookList));
        updateQuery.setUpdateRequest(request);
        elasticsearchTemplate.update(updateQuery);
    }

    @Override
    public List<BookList> getByBookIdAndSort(String bookId, int maxlastInfo) {
        UrlUtils.urlAddNotHtml(bookId,String.valueOf(maxlastInfo));
        QueryBuilder qb1 = QueryBuilders.termQuery("bookId",bookId);
        // 大于
//        QueryBuilder qb2 = QueryBuilders.matchQuery("sort",maxlastInfo);
        QueryBuilder qb2 = QueryBuilders.rangeQuery("sort").gt(maxlastInfo);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1).must(qb2);

        Iterable aIterable = bookListRepository.search(qb);
        return Lists.newArrayList(aIterable);
    }

    @Override
    public PageHelper getQuery(String type, String value, String sort, PageHelper pageHelper) {
        if (pageHelper.getPage().intValue() < 1) {
            pageHelper.setPage(1);
        }
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();

        QueryBuilder qb1 = QueryBuilders.termQuery(type,value);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        if (StringUtils.equalsIgnoreCase(sort,SortOrder.ASC.toString())) {
            searchQueryBuilder.withSort(SortBuilders.fieldSort("sort").order(SortOrder.ASC));
        }else {
            searchQueryBuilder.withSort(SortBuilders.fieldSort("sort").order(SortOrder.DESC));
        }
        searchQueryBuilder.withQuery(qb);

        searchQueryBuilder.withPageable(PageRequest.of(pageHelper.getPage() - 1,pageHelper.getLimit()));
        Page<BookList> page = bookListRepository.search(searchQueryBuilder.build());
        List<BookList> lists = page.getContent();
        PageHelper helper = pageHelper.pagination(Integer.valueOf(String.valueOf(page.getTotalElements())),
                pageHelper.getLimit(), pageHelper.getPage(), lists);

        return helper;
    }

    @Override
    public List<BookList> getById(String bookId) {
        QueryBuilder qb1 = QueryBuilders.termQuery("bookId",bookId);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookListRepository.search(qb);
        return Lists.newArrayList(aIterable);
    }


    @Override
    public BookList getByReadUrl(String readUrl) {
        QueryBuilder qb1 = QueryBuilders.termQuery("readUrl.keyword",readUrl);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookListRepository.search(qb);
        List<BookList> list = Lists.newArrayList(aIterable);
        return list.get(0);
    }

}
