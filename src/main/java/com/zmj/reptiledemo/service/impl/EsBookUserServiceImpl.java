package com.zmj.reptiledemo.service.impl;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.mapper.BookUserRepository;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.service.EsBookUserService;
import com.zmj.reptiledemo.utils.MD5Util;
import com.zmj.reptiledemo.utils.RandomUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class EsBookUserServiceImpl implements EsBookUserService {

    @Autowired
    private BookUserRepository bookUserRepository;

    @Override
    public void save(BookUser bookUser) {
        bookUser.setCreateTime(new Date());
        bookUser.setLastTime(new Date());
        String salt = RandomUtils.generateString(10);
        bookUser.setSalt(salt);
        bookUser.setNickName(RandomUtils.generateString(8) + "号");
        String password = MD5Util.getMD5Str(bookUser.getPassword(), salt);
        bookUser.setPassword(password);
        bookUserRepository.save(bookUser);
    }

    @Override
    public void login(BookUser bookUser) {
        bookUser.setLastTime(new Date());
        bookUserRepository.save(bookUser);
    }

    @Override
    public List<BookUser> query(String email) {
        QueryBuilder qb1 = QueryBuilders.termQuery("email.keyword",email);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookUserRepository.search(qb);
        List<BookUser> list = Lists.newArrayList(aIterable);
        return list;
    }
}
