package com.zmj.reptiledemo.service.impl;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.mapper.BookShelfRepository;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookShelf;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.service.EsBookService;
import com.zmj.reptiledemo.service.EsBookShelfService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class EsBookShelfServiceImpl implements EsBookShelfService {

    @Autowired
    private BookShelfRepository bookShelfRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private EsBookService bookService;

    @Override
    public void save(String email,String bookId) {
        Book book = bookService.getQuery("id", bookId).get(0);
        BookShelf shelf = new BookShelf();
        shelf.setBookId(bookId);
        shelf.setAuthor(book.getAuthor());
        shelf.setBookName(book.getBookName());
        shelf.setEmail(email);
        shelf.setCreateTime(new Date());
        shelf.setStatus(0);
        bookShelfRepository.save(shelf);
    }

    @Override
    public List<BookShelf> query(String email) {
        QueryBuilder qb1 = QueryBuilders.termQuery("email",email);
        QueryBuilder qb2 = QueryBuilders.termQuery("status",0);
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(qb1).withQuery(qb2).build();
        List<BookShelf> bookShelfList = elasticsearchTemplate.queryForList(searchQuery, BookShelf.class);
        return bookShelfList;
    }

    @Override
    public void under(String email,String bookId) {
        Book book = bookService.getQuery("id", bookId).get(0);
        BookShelf shelf = new BookShelf();
        shelf.setBookId(bookId);
        shelf.setAuthor(book.getAuthor());
        shelf.setBookName(book.getBookName());
        shelf.setEmail(email);
        shelf.setStatus(1);
        bookShelfRepository.save(shelf);
    }

    @Override
    public BookShelf queryByBookId(String bookId) {
        QueryBuilder qb1 = QueryBuilders.termQuery("bookId",bookId);
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(qb1).build();
        List<BookShelf> bookShelfList = elasticsearchTemplate.queryForList(searchQuery, BookShelf.class);
        if (bookShelfList == null || bookShelfList.size() == 0) {
            return null;
        }
        return bookShelfList.get(0);
    }

    @Override
    public void update(BookShelf bookShelf) {
        bookShelfRepository.save(bookShelf);
    }

}
