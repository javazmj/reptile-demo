package com.zmj.reptiledemo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.constant.CategoryConstant;
import com.zmj.reptiledemo.enums.CategoryEnum;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.mapper.BookRepository;
import com.zmj.reptiledemo.redis.RedisService;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.service.EsBookService;
import com.zmj.reptiledemo.utils.http.HttpClientUtils;
import com.zmj.reptiledemo.utils.JsonUtil;
import com.zmj.reptiledemo.utils.ResultVOUtil;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import com.zmj.reptiledemo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class EsBookServiceImpl implements EsBookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private RedisService redisService;

    @Autowired
    private StringRedisTemplate redisTemplate;


    /**
     * 批量新增
     **/
    @Override
    public void batchAddBook(List<Book> Books) {
        if(CollectionUtils.isEmpty(Books)) {
            return ;
        }
        List<IndexQuery> queries = Lists.newArrayListWithExpectedSize(Books.size());
        IndexQuery indexItem  = null;
        for(Book Book :Books) {
            indexItem = new IndexQuery();
            indexItem.setObject(Book);
            queries.add(indexItem);
        }
        elasticsearchTemplate.bulkIndex(queries);
    }

    @Override
    public void addBook(Book book) {
        if (getById(book.getId()) == 0) {
            bookRepository.save(book);
        }else {
            log.info("该小说已经存在,{}",book.getId());
        }
    }

    @Override
    public void deletedBookById(String id) {
        bookRepository.deleteById(id);
    }

    /**
     * 根据BookId更新信息
     */
    @Override
    public void updateBook(Book book) {
        UpdateQuery updateQuery = new UpdateQuery();
        updateQuery.setId(book.getId());
        updateQuery.setClazz(Book.class);
        book.setId(null);
        UpdateRequest request = new UpdateRequest();
        request.doc(JsonUtil.toJson(book));
        updateQuery.setUpdateRequest(request);
        elasticsearchTemplate.update(updateQuery);
    }

    @Override
    public List<Book> getQuery(String type, Object name) {
        QueryBuilder qb1 = QueryBuilders.termQuery(type,name);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookRepository.search(qb);
        return Lists.newArrayList(aIterable);
    }

    @Override
    public int getById(String bookId) {
        QueryBuilder qb1 = QueryBuilders.termQuery("id",bookId);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookRepository.search(qb);
        ArrayList list = Lists.newArrayList(aIterable);
        if (list != null && list.size() > 0){
            return 1;
        }
        return 0;
    }

    @Override
    public List<Book> queryAll() {
        QueryBuilder qb = QueryBuilders.queryStringQuery("*");
        Iterable aIterable = bookRepository.search(qb);
        List list = Lists.newArrayList(aIterable);
        return list;
    }

    @Override
    public List<Book> querySearch(String search) {
        QueryBuilder queryBuilder1 = QueryBuilders.wildcardQuery("bookName", "*" + search + "*");
        QueryBuilder queryBuilder2 = QueryBuilders.wildcardQuery("author", "*" + search + "*");
        QueryBuilder qb = QueryBuilders.boolQuery().should(queryBuilder1).should(queryBuilder2);

        Iterable aIterable = bookRepository.search(qb);
        return Lists.newArrayList(aIterable);
    }

    /**
     * 站外搜索
     * @param search
     * @return
     */
    @Override
    public ResultVO searchSite(String search) {
        String baseUrl  = BaseLinks.searchUrl + search;

        try {
            Document searchDoc = HttpClientUtils.reptile(baseUrl);
            Elements select = searchDoc.getElementsByAttributeValue("class","item");
            if (select != null && select.size() > 0) {
                for (Element e: select) {
                    Elements iamge = e.getElementsByAttributeValue("class","image");
                    String id = iamge.select("a[href]").get(0).attr("href");
                    String trimId = UrlUtils.getTrimId(id);
                    //如果存在此id的小说
                    int byId = getById(trimId);
                    if (byId == 0) {
                        redisTemplate.opsForSet().add(BaseLinks.SEARCE_LINK_REDIS_PREFIX,id);
                    }
                }
                return ResultVOUtil.error(ResultEnum.SEARCH_WAITING_BOOK.getMsg());
            }else {
                return ResultVOUtil.error( ResultEnum.NOE_BOOK.getMsg());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Book> getRedisBookRandom(int code,String category,int n) {

        if (!redisService.exists(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + code)) {

            List<Book> list = queryAll();
            categorySetBook(list);

        }
        List<String> strs = redisService.srandmember(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + code, n);
        return JSONArray.parseArray(strs.toString(), Book.class);
    }

    @Override
    public List<Book> getRedisBookAll(int code) {
        Set<String> set = redisService.smembers(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + code);
        return JSONArray.parseArray(set.toString(), Book.class);
    }

    private void categorySetBook(List<Book> list) {

        for (Book book : list) {
            String jsonString = JSONObject.toJSONString(book);
            if (book.getCategory().equals(CategoryConstant.xh)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.XUANHUA.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.xz)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.XIUZHEN.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.ls)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.LISHI.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.ds)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.DUSHI.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.wy)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.WANGYOU.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.kh)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.KEHUAN.getCode(), jsonString);

            }else if(book.getCategory().equals(CategoryConstant.np)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.NVPIN.getCode(), jsonString);
            }

            if (book.getStatus().equals(CategoryConstant.wb)) {
                redisService.sadd(BaseLinks.EDIS_BOOKLIST_CACHE,BaseLinks.REDIS_BOOKLIST + CategoryEnum.WANBEN.getCode(), jsonString);
            }
        }

        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.XUANHUA.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.XIUZHEN.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.DUSHI.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.LISHI.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.WANGYOU.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.KEHUAN.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.NVPIN.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);
        redisService.expire(BaseLinks.EDIS_BOOKLIST_CACHE, BaseLinks.REDIS_BOOKLIST + CategoryEnum.WANBEN.getCode(), BaseLinks.EDIS_BOOKLIST_EXPIRE);

    }
}
