package com.zmj.reptiledemo.service.impl;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.mapper.BookInfoRepository;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.service.EsBookInfoService;
import com.zmj.reptiledemo.utils.JsonUtil;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class EsBookInfoServiceImpl implements EsBookInfoService {

    @Autowired
    private BookInfoRepository bookInfoRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 批量新增
     **/
    @Override
    public void batchAddBookInfo(List<BookInfo> bookInfos) {
        if(CollectionUtils.isEmpty(bookInfos)) {
            return ;
        }
        List<IndexQuery> queries = Lists.newArrayListWithExpectedSize(bookInfos.size());
        IndexQuery indexItem  = null;
        for(BookInfo bookInfo :bookInfos) {
            indexItem = new IndexQuery();
            indexItem.setObject(bookInfo);
            queries.add(indexItem);
        }
        elasticsearchTemplate.bulkIndex(queries);
    }

    @Override
    public void addBookInfo(BookInfo bookInfo,boolean exit) {
        if (true) {
            if (getById(bookInfo.getReadUrl()) == 0) {
                bookInfoRepository.save(bookInfo);
            }else {
                log.info("该小说章节已经存在,{}",bookInfo.getReadUrl());
            }
        }else {
            bookInfoRepository.save(bookInfo);
        }

    }

    @Override
    public void deletedBookInfoById(String id) {
        bookInfoRepository.deleteById(id);
    }

    /**
     * 根据BookListId更新信息
     */
    @Override
    public void updateBookInfo(BookInfo bookInfo) {
        UpdateQuery updateQuery = new UpdateQuery();
        updateQuery.setId(bookInfo.getBookId());
        updateQuery.setClazz(BookList.class);
        bookInfo.setBookId(null);
        UpdateRequest request = new UpdateRequest();
        request.doc(JsonUtil.toJson(bookInfo));
        updateQuery.setUpdateRequest(request);
        elasticsearchTemplate.update(updateQuery);
    }


    @Override
    public List<BookInfo> getByBookIdAndSort(String bookId, String  sort) {
        String readUrl = "";
        if (!sort.contains(".")) {
            readUrl = UrlUtils.urlAdd(bookId,Integer.parseInt(sort));
        }else {
            readUrl = UrlUtils.urlAddNotHtml(bookId,sort);
        }
        QueryBuilder qb1 = QueryBuilders.termQuery("readUrl.keyword",readUrl);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookInfoRepository.search(qb);
        return Lists.newArrayList(aIterable);
    }


    @Override
    public int getById(String readUrl) {
        QueryBuilder qb1 = QueryBuilders.termQuery("readUrl.keyword",readUrl);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable aIterable = bookInfoRepository.search(qb);
        ArrayList list = Lists.newArrayList(aIterable);
        if (list != null && list.size() > 0){
            return 1;
        }
        return 0;
    }

    @Override
    public List<BookInfo> getByBookId(String trimId) {

        QueryBuilder qb1 = QueryBuilders.termQuery("bookId",trimId);
        QueryBuilder qb = QueryBuilders.boolQuery().must(qb1);
        Iterable<BookInfo> search = bookInfoRepository.search(qb);
        return Lists.newArrayList(search);
    }


}
