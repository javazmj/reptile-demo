package com.zmj.reptiledemo.service;

import com.zmj.reptiledemo.repository.BookShelf;

import java.util.List;

public interface EsBookShelfService {

    void save(String email,String bookId);

    List<BookShelf> query(String email);

    void under(String email,String bookId);

    BookShelf queryByBookId(String bookId);

    void update(BookShelf bookShelf);
}
