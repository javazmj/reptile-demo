package com.zmj.reptiledemo.service;

import com.zmj.reptiledemo.repository.BookUser;

import java.util.List;

public interface EsBookUserService {

    void save(BookUser bookUser);

    void login(BookUser bookUser);

    List<BookUser> query(String email);

}
