package com.zmj.reptiledemo.service;

import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.utils.PageHelper;

import java.util.List;

public interface EsBookListService {

    void batchAddBookList(List<BookList> bookLists);

    void addBookList(BookList bookList);

    void deletedBookListById(String id);

    void updateBookList(BookList bookList);

    List<BookList> getByBookIdAndSort(String bookId, int maxlastInfo);

    PageHelper getQuery(String type, String bookId, String sort, PageHelper page);

    BookList getByReadUrl(String readUrl);

    List<BookList> getById(String bookId);
}
