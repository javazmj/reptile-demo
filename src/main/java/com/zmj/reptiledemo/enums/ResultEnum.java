package com.zmj.reptiledemo.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {

    SUCCESS(0,"成功"),
    ERROR(-1,"失败"),

    LOGIN_FAIL(10010,"登录失败,账号或密码错误"),
    SEND_MAIL_ERROR(10011,"邮件发送失败,请重试"),
    SEND_MAIL_SUCCESS(10011,"已发送验证码到您的邮箱,请注意查收"),
    VERIFCODE_ERROR(10012,"验证码错误"),
    NOT_LOGIN(10013,"账号未登录"),
    EMAIL_EXISTS(10014,"该邮箱已注册"),
    SESSION_ERROR(10015,"session错误"),
    ACCESSLIMT(10016,"短时间内请求次数过多"),


    SEARCH_WAITING_BOOK(10017,"已找到相关小说,等待热心网友分享,需要一点点时间..."),
    SEARCH_BOOKS_WIN(10018,"已分享小说章节目录,正文请稍等..."),
    SEARCH_BOOKINFO_WIN(10019,"已分享小说章节正文"),
    NOE_BOOK(10020,"未搜索到您查找的小说"),
    SEARCH_WIN(10021,"搜索的小说正文已全部入库,重新搜索即可"),


    REGISTERY_SUCCESS(20010,"注册成功"),
    LOGIN_SUCCESS(20011,"登录成功"),
    BOOK_SHELF_NOT(20012,"快去添加一本喜欢的小说吧"),


    ADD_BOO_SHELF(20013,"加入书架成功"),
    UNDER_BOO_SHELF(20014,"取消书架成功"),


    ;



    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
