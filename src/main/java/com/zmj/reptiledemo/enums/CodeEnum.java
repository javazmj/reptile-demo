package com.zmj.reptiledemo.enums;

public interface CodeEnum {

    Integer getCode();

    String getText();

}
