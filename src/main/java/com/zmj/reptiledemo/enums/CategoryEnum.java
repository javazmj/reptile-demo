package com.zmj.reptiledemo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CategoryEnum implements CodeEnum{

    XUANHUA(3,"玄幻小说"),
    XIUZHEN(4,"修真小说"),
    DUSHI(5,"都市小说"),
    LISHI(6,"历史小说"),
    WANGYOU(7,"网游小说"),
    KEHUAN(8,"科幻小说"),
    NVPIN(9,"女生频道"),
//    PAIHANGBANG(1,"排行榜单"),
    WANBEN(2,"完本小说"),

    ;

    private Integer code;
    private String text;


    @Override
    public String getText() {
        return text;
    }

    @Override
    public Integer getCode() {
        return code;
    }


    CategoryEnum(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (CategoryEnum ces : CategoryEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", ces.getCode());
            map.put("text", ces.getText());
            list.add(map);
        }
        return list;
    }

    public static String getByCode(Integer code) {
        for (CategoryEnum ces : CategoryEnum.values()) {
            if (code.intValue() == ces.getCode().intValue()) {
                return ces.getText();
            }
        }
        return null;
    }

}
