package com.zmj.reptiledemo.interceptor;

import com.zmj.reptiledemo.repository.BookUser;

public class UserContext {

    private static ThreadLocal<BookUser> userHolder = new ThreadLocal<BookUser>();

    public static void setUser(BookUser bookUser) {
        userHolder.set(bookUser);
    }

    public static BookUser getUser() {
        return userHolder.get();
    }
}
