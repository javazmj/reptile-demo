package com.zmj.reptiledemo.repository;

import lombok.Data;
import org.apache.lucene.codecs.FieldInfosFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

@Data
@Document(indexName = "novel",type = "book")
public class Book implements Serializable {

    private static final long serialVersionUID = 8058846183802946244L;

    @Id
    private String id;

    private String description;

    private String image;

    private String category;

    @Field(type= FieldType.keyword)
    private String author;

    @Field(type= FieldType.keyword)
    private String bookName;

    private String readUrl;

    private String status;

    @Field(type=FieldType.Date)
    private Date updateTime;

    @Field(type=FieldType.Date)
    private Date createTime;

    private String latestChapterName;

    private String latestChapterUrl;

}