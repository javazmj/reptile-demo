package com.zmj.reptiledemo.repository;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Data
@Document(indexName = "novel",type = "bookinfo")
public class BookInfo implements Serializable {

    private static final long serialVersionUID = 7211837590337550637L;

    private String bookId;

    @Id
    private String readUrl;

    private String pre;

    private String next;

    private String content;

}