package com.zmj.reptiledemo.repository;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

@Data
@Document(indexName = "novel",type = "booklist")
public class BookList implements Serializable {

    private static final long serialVersionUID = 4252922287795414078L;

    private String bookId;

    @Id
    private String readUrl;

    private String name;

    @Field(type= FieldType.Integer)
    private Integer sort;

    private String content;

}