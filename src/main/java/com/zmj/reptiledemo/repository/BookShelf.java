package com.zmj.reptiledemo.repository;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

@Data
@Document(indexName = "novel",type = "bookshelf")
public class BookShelf implements Serializable {

    private static final long serialVersionUID = 3961313443412794213L;

    @Id
    private String bookId;

    private String bookName;

    private String author;

    private String email;

    private String readUrl;

    @Field(type=FieldType.Date)
    private Date createTime;

    //0加入书架 1删除
    private int status = 0;


}