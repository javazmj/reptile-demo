package com.zmj.reptiledemo.repository;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

/**
 * @author meijun
 */
@Data
@Document(indexName = "novel",type = "bookuser")
public class BookUser implements Serializable {

    private static final long serialVersionUID = 7736709181902725920L;

    @Id
    private String email;

    private String mobile;

    private String password;

    private String nickName;

    private String head;

    @JSONField(serialize=false)
    private String salt;

    private String lastIp;

    @Field(type= FieldType.Date)
    private Date createTime;

    private Date lastTime;

}