package com.zmj.reptiledemo.repository;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

@Data
@Document(indexName = "novel",type = "apilog")
public class ApiLog implements Serializable {

    private static final long serialVersionUID = -6725300416068402494L;

    @Id
    private String id;

    private String controller;

    private String method;

    private String params;

    private String ip;

    @Field(type= FieldType.Date)
    private Date requestTime;

    private String user;

    private long execute;
}