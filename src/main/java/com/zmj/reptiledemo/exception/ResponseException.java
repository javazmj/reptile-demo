package com.zmj.reptiledemo.exception;

import com.zmj.reptiledemo.enums.ResultEnum;

public class ResponseException extends RuntimeException {


    private Integer code;

    public ResponseException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public ResponseException(String message) {
        super(message);
        this.code = -1;
    }
}
