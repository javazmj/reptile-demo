package com.zmj.reptiledemo.exception;

import com.zmj.reptiledemo.enums.ResultEnum;

public class RequestLimitException extends RuntimeException {

    public RequestLimitException(){

        super("HTTP请求超出设定的限制!");
    }

    private Integer code;

    public RequestLimitException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public RequestLimitException(String message) {
        super(message);
        this.code = -1;
    }
}
