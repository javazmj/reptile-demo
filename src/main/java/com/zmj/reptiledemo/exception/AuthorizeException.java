package com.zmj.reptiledemo.exception;

import com.zmj.reptiledemo.enums.ResultEnum;

public class AuthorizeException extends RuntimeException {


    private Integer code;

    public AuthorizeException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public AuthorizeException(String message) {
        super(message);
        this.code = -1;
    }
}
