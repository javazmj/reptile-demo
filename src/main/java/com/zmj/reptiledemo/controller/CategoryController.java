package com.zmj.reptiledemo.controller;

import com.zmj.reptiledemo.enums.CategoryEnum;
import com.zmj.reptiledemo.interceptor.AccessLimit;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.service.EsBookService;
import com.zmj.reptiledemo.utils.EnumUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private EsBookService esBookService;

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @GetMapping("/{code}")
    public String categoryBy(Model model, @PathVariable("code") Integer code) {
        String text = EnumUtil.getByCode(code, CategoryEnum.class);
        List<Book> redisBook = esBookService.getRedisBookAll(code);
        model.addAttribute("list",redisBook);
        model.addAttribute("cate",text);
        return "views/categorybook";
    }

    @GetMapping
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public String category(Model model) {
        List<Book> redisBook = esBookService.getRedisBookAll(CategoryEnum.WANBEN.getCode());
        model.addAttribute("list",redisBook);
        model.addAttribute("cate",CategoryEnum.WANBEN.getText());
        return "views/wanben";
    }



}
