package com.zmj.reptiledemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.enums.CategoryEnum;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.interceptor.AccessLimit;
import com.zmj.reptiledemo.redis.RedisService;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.repository.BookShelf;
import com.zmj.reptiledemo.scheduled.AsyncBookInfo;
import com.zmj.reptiledemo.service.EsBookInfoService;
import com.zmj.reptiledemo.service.EsBookListService;
import com.zmj.reptiledemo.service.EsBookService;
import com.zmj.reptiledemo.service.EsBookShelfService;
import com.zmj.reptiledemo.utils.LambdaValid;
import com.zmj.reptiledemo.utils.PageHelper;
import com.zmj.reptiledemo.utils.ResultVOUtil;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import com.zmj.reptiledemo.vo.ResultVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping
public class BookController {


    @Autowired
    private EsBookInfoService esbookInfoService;

    @Autowired
    private EsBookListService esbookListService;

    @Autowired
    private EsBookService esbookService;

    @Autowired
    private EsBookShelfService esBookShelfService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private AsyncBookInfo asyncBookInfo;

    @GetMapping
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public String get(Model model) {
        List<Map<String, Object>> list = CategoryEnum.toList();
        for (Map<String,Object> map : list) {
            String text = String.valueOf(map.get("text"));
            List<Book> books = null;
            if (!text.equals("完本小说")) {
                int code = Integer.parseInt(map.get("code").toString());
                books = esbookService.getRedisBookRandom(code,text,6);
            }
            if (books != null && books.size() > 0) {
                map.put("books",books);
            }
        }
        list.removeIf(e -> LambdaValid.isValid(e));
        model.addAttribute("list",list);
        return "views/index";
    }

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @GetMapping("/{bookId}")
    public String bookList(Model model, @PathVariable("bookId")String bookId, @RequestParam(defaultValue = "ASC") String sort, PageHelper page) {
        List<Book> book = esbookService.getQuery("id",bookId);
        if (book == null || book.size() == 0) {
            return "error/404";
        }
        PageHelper pageHelper = esbookListService.getQuery("bookId",book.get(0).getId(),sort,page);
        model.addAttribute("book",book.get(0));
        model.addAttribute("page",pageHelper);
        model.addAttribute("sort",sort);
        return "views/booklist";
    }

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @GetMapping("/{bookId}/{readUrl}")
    public String bookinfo(Model model, String email, HttpServletRequest request, @PathVariable("bookId") String bookId, @PathVariable("readUrl") String readUrl) {
        if (StringUtils.isNotBlank(email)) {
            //如果是登录会员,则记录阅读到多少章节
            BookShelf bookShelf = esBookShelfService.queryByBookId(bookId);
            if (null != bookShelf) {
                bookShelf.setReadUrl(readUrl);
                esBookShelfService.update(bookShelf);
            }
        }

        String newReadUrl = "";
        if (!readUrl.contains(".")) {
            newReadUrl = UrlUtils.urlAdd(bookId,Integer.parseInt(readUrl));
        }else {
            newReadUrl = UrlUtils.urlAddNotHtml(bookId,readUrl);
        }
        List<Book> book = esbookService.getQuery("id",bookId);
        if (null == book || book.size() ==0) {
            return "error/404";
        }
        model.addAttribute("book",book.get(0));

        BookList  bookLists = esbookListService.getByReadUrl(newReadUrl);
        model.addAttribute("bookList",bookLists);

        BookInfo bookInfo = null;

        if (!redisService.exists(RedisConstant.BOOK_INFO_CACHE_INDEX,newReadUrl)) {
            List<BookInfo> bookInfos = esbookInfoService.getByBookIdAndSort(bookId, readUrl);

            if (null == bookInfos || bookInfos.size() ==0) {
                return "redirect:";
            }
            model.addAttribute("bookInfo",bookInfos.get(0));

            asyncBookInfo.info(bookId,bookInfos.get(0).getNext());
        }else {
            String s = redisService.get(RedisConstant.BOOK_INFO_CACHE_INDEX,
                    newReadUrl);
            bookInfo = JSONObject.parseObject(s,BookInfo.class);
            model.addAttribute("bookInfo",bookInfo);
        }

        return "views/bookinfo";
    }

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @PostMapping("/search")
    public String search(Model model,String email,String search) {
        List<Book> books = esbookService.querySearch(search);
        model.addAttribute("search",search);
        model.addAttribute("code",0);
        model.addAttribute("list",books);
//        //未搜索到
        if (books == null || books.size() == 0) {
            ResultVO resultVO = esbookService.searchSite(search);
            model.addAttribute("searchMsg",resultVO.getMsg());
            model.addAttribute("code",resultVO.getCode());
        }
        return "views/searchbook";
    }


    /**
     * 并不实用 花里胡哨
     * @param n
     * @param email
     * @param search
     * @return
     */
    @AccessLimit(seconds=60,maxCount=200,needLogin=false)
    @PostMapping("/realSearch")
    @ResponseBody
    public ResultVO realSearch(String n, String email, String search) {
        List<Book> books = esbookService.querySearch(search);
        //未搜索到
        if (books == null || books.size() == 0) {
            return ResultVOUtil.error(ResultEnum.SEARCH_WAITING_BOOK.getMsg());
        } else {
            for (Book b : books) {
                List<BookList> bookLists = esbookListService.getById(b.getId());
                if (bookLists != null && bookLists.size() > 0) {
                    List<BookInfo> bookInfos = esbookInfoService.getByBookId(b.getId());
                    if (bookInfos != null && bookInfos.size() > 0) {
                        if (bookInfos.size() == bookLists.size()) {
                            return ResultVOUtil.success(ResultEnum.SEARCH_WIN.getMsg());
                        }else {
                            return ResultVOUtil.error(ResultEnum.SEARCH_BOOKS_WIN.getMsg());
                        }
                    }
                }
                return ResultVOUtil.error(ResultEnum.SEARCH_BOOKS_WIN.getMsg());
            }
            return ResultVOUtil.error(ResultEnum.SEARCH_WAITING_BOOK.getMsg());
        }
    }


}
