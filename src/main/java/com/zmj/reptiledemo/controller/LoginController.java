package com.zmj.reptiledemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.exception.AuthorizeException;
import com.zmj.reptiledemo.exception.ResponseException;
import com.zmj.reptiledemo.interceptor.AccessLimit;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookShelf;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.service.EsBookShelfService;
import com.zmj.reptiledemo.service.EsBookUserService;
import com.zmj.reptiledemo.service.SendMailService;
import com.zmj.reptiledemo.utils.CookieUtil;
import com.zmj.reptiledemo.utils.IpUtil;
import com.zmj.reptiledemo.utils.MD5Util;
import com.zmj.reptiledemo.utils.ResultVOUtil;
import com.zmj.reptiledemo.vo.ResultVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private SendMailService sendMailService;

    @Autowired
    private EsBookUserService bookUserService;

    @Autowired
    private EsBookShelfService bookShelfService;


    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @GetMapping
    public String login() {
        return "views/login";
    }

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @GetMapping("/register")
    public String register() {
        return "views/register";
    }

    @PostMapping
    @ResponseBody
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public ResultVO loginForm(HttpServletRequest request,HttpServletResponse response,BookUser bookUser) {
        List<BookUser> users = bookUserService.query(bookUser.getEmail());
        if (users != null && users.size() > 0) {
            BookUser user = users.get(0);
            String md5Str = MD5Util.getMD5Str(bookUser.getPassword(), user.getSalt());
            if (StringUtils.equals(md5Str,user.getPassword())) {
                String ipAddr = IpUtil.getIpAddr(request);
                bookUser.setLastIp(ipAddr);
                bookUserService.login(user);
                setToken(response,bookUser);
                return ResultVOUtil.successMsg(ResultEnum.LOGIN_SUCCESS.getMsg());
            }
        }
        return ResultVOUtil.error("账号或密码错误");
    }

    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    @PostMapping("/register")
    @ResponseBody
    public ResultVO registerForm(HttpServletRequest request, HttpServletResponse response,
                                 BookUser bookUser, String verifCode) {
        Boolean aBoolean = redisTemplate.hasKey(bookUser.getEmail());
        if (aBoolean) {
            String redisVerifCode = redisTemplate.opsForValue().get(bookUser.getEmail());
            if (redisVerifCode.equals(verifCode)) {
                bookUser.setLastIp(IpUtil.getIpAddr(request));
                bookUserService.save(bookUser);
                setToken(response,bookUser);
                return ResultVOUtil.successMsg(ResultEnum.REGISTERY_SUCCESS.getMsg());
            }
        }
        return ResultVOUtil.error(ResultEnum.VERIFCODE_ERROR.getMsg());
    }

    @GetMapping("/registerSend")
    @ResponseBody
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public ResultVO registerSend(@RequestParam(required = true) String email) {
        try {
            sendMailService.sendRegisterHtmlMail(email);
        } catch (MessagingException e) {
            return ResultVOUtil.error(ResultEnum.SEND_MAIL_ERROR.getMsg());
        }
        return ResultVOUtil.successMsg(ResultEnum.SEND_MAIL_SUCCESS.getMsg());

    }

    @GetMapping("/checkEmail")
    @ResponseBody
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public ResultVO checkEmail(String email) {
        List<BookUser> query = bookUserService.query(email);
        if (query != null && query.size() > 0) {
            return ResultVOUtil.error(ResultEnum.EMAIL_EXISTS.getMsg());
        }
        return ResultVOUtil.success();
    }


    @GetMapping("/my")
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public String my(Model model,String email,HttpServletRequest request,HttpServletResponse response) {

        List<BookUser> users = bookUserService.query(email);
        if (users != null && users.size() > 0) {
            //延长cookie 不更换
            Cookie cookie = CookieUtil.get(request, RedisConstant.COOKIE_TOKEN);
            Integer expire = RedisConstant.EXPIRE;
            //设置token到redis
            redisTemplate.opsForValue().set(String.format(RedisConstant.TOKEN_PREFIX,cookie.getValue()),JSONObject.toJSONString(users.get(0)), expire, TimeUnit.SECONDS);
            //设置token到cookie
            CookieUtil.set(response, RedisConstant.COOKIE_TOKEN, cookie.getValue(), expire);
        }

        List<BookShelf> query = bookShelfService.query(email);
        if (query == null || query.size() == 0) {
            model.addAttribute("msg",ResultEnum.BOOK_SHELF_NOT.getMsg());
        }
        model.addAttribute("email",email);
        model.addAttribute("list",query);
        return "views/my";
    }

    @PostMapping("/addShelf")
    @ResponseBody
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public ResultVO addShelf(String n,String email,String bookId) {
        bookShelfService.save(email,bookId);

        return ResultVOUtil.successMsg(ResultEnum.ADD_BOO_SHELF.getMsg());
    }


    @PostMapping("/under")
    @ResponseBody
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public ResultVO under(String n,String email,String bookId) {
        bookShelfService.under( email, bookId);
        return ResultVOUtil.successMsg(ResultEnum.UNDER_BOO_SHELF.getMsg());
    }


    @GetMapping("/500")
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public String error500() {
        return "/error/500";
    }

    @GetMapping("/404")
    @AccessLimit(seconds=5,maxCount=15,needLogin=false)
    public String error404() {
        return "/error/404";
    }

    private void setToken(HttpServletResponse response, BookUser bookUser) {
        String token = UUID.randomUUID().toString();
        Integer expire = RedisConstant.EXPIRE;
        //设置token到redis
        redisTemplate.opsForValue().set(String.format(RedisConstant.TOKEN_PREFIX,token),JSONObject.toJSONString(bookUser), expire, TimeUnit.SECONDS);
        //设置token到cookie
        CookieUtil.set(response, RedisConstant.COOKIE_TOKEN, token, expire);
    }
}
