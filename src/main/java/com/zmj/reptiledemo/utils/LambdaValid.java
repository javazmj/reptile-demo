package com.zmj.reptiledemo.utils;

import java.util.Map;

public class LambdaValid {

    /**
     * @param map
     * @return
     */
    public static boolean isValid(Map<String, Object> map) {
        if (String.valueOf(map.get("code").toString()).equals("1") || String.valueOf(map.get("code").toString()).equals("2") ) {
            return true;
        }
        return false;
    }

    public static boolean isNotValid(Map<String, Object> map) {
        return !isValid(map);
    }

}
