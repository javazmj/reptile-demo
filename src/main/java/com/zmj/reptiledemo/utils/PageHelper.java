package com.zmj.reptiledemo.utils;

import lombok.Data;

import java.util.List;

/**
 * 分页
 *
 * @author
 * @since 2018/11/22 19:01
 */
@Data
public class PageHelper {
    // 总条数
    private Integer totalCount;
    // 总页数
    private Integer totalPages;
    // 每页条数
    private Integer limit = 50;
    // 当前页码从1开始
    private Integer page = 1;
    // 前一页
    private Integer prePage;
    // 是否首页
    private Boolean firstPage;
    // 是否末页
    private Boolean lastPage;
    // 下一页
    private Integer nextPage;
    // 偏移量
    private Integer offset;
    // 是否有下页
    private Boolean hasNextPage;
    // 是否有上页
    private Boolean hasPrePage;
    // 当前页从第几条开始查
    private Integer startRow;
    // 返回数据
    private List items;

    public PageHelper pagination(Integer totalCount, Integer limit, Integer page, List items) {
        PageHelper pageHelper = new PageHelper();
        pageHelper.setTotalCount(totalCount);
        Integer totalPage = totalCount % limit == 0 ? totalCount / limit : totalCount / limit + 1;
        pageHelper.setTotalPages(totalPage);
        pageHelper.setPage(page);
        pageHelper.setLimit(limit);
        pageHelper.setOffset(limit);
        pageHelper.setStartRow(limit * (page - 1));
        if (page > 1 && page < totalPage) {
            firstPage = false;
            hasPrePage = true;
            nextPage = page + 1;
            prePage = page - 1;
        } else if (page == totalPage && page != 1) {
            lastPage = true;
            hasNextPage = false;
            firstPage = false;
            hasPrePage = true;
            prePage = page - 1;
        } else if (page == 1 && page != totalPage) {
            lastPage = false;
            hasNextPage = true;
            firstPage = true;
            hasPrePage = false;
            nextPage = page + 1;
        } else if (page == totalPage && totalPage == 1) {
            lastPage = true;
            hasNextPage = false;
            firstPage = true;
            hasPrePage = false;
        }
        pageHelper.setLastPage(lastPage);
        pageHelper.setFirstPage(firstPage);
        pageHelper.setHasNextPage(hasNextPage);
        pageHelper.setHasPrePage(hasPrePage);
        pageHelper.setPrePage(prePage);
        pageHelper.setNextPage(nextPage);
//        int toIndex = 0;
//        if (pageHelper.getStartRow() + pageHelper.getLimit() >= pageHelper.getTotalCount()) {
//            toIndex = pageHelper.getTotalCount();
//        } else {
//            toIndex = pageHelper.getStartRow() + pageHelper.getLimit();
//        }
//        if (pageHelper.getStartRow() > toIndex) {
//            this.items = null;
//        } else {
//            this.items = items.subList(pageHelper.getStartRow(), toIndex);
//        }
        pageHelper.setItems(items);
        return pageHelper;
    }

}
