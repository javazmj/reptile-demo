package com.zmj.reptiledemo.utils.http;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;

/**
 * auth: zmj
 * date: 2019/06/09
 */
public class HttpClientUtil {
    private static final int timeOut = 20000;


    public static Document getRequest(String url) throws Exception{
        return getRequest(url,timeOut);
    }
    public static Document getRequest(String url, int timeOut) throws Exception{
        URL u = new URL(url);
        if("https".equalsIgnoreCase(u.getProtocol())){
            SslUtils.ignoreSsl();
        }

        Connection tempConn = Jsoup.connect(url);
        //模拟浏览器的请求头
        tempConn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36");
        tempConn.timeout(timeOut);
        //开始连接HTTP请求。
        Connection.Response demo = tempConn.ignoreContentType(true).method(Connection.Method.GET)
                .execute();
        return Jsoup.parse(demo.body());
    }
}
