package com.zmj.reptiledemo.utils;


import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.vo.ResultVO;

/**
 * Created by: meijun
 * Date: 2017/12/19 14:29
 */
public class ResultVOUtil {

    private static int success_code = 0;
    private static int error_code = -1;

    public  static ResultVO success (Object o) {
        return success(o,"成功");
    }

    public  static ResultVO success (Object o,String msg) {
        ResultVO resultVO = new ResultVO();
        resultVO.setData(o);
        resultVO.setCode(success_code);
        resultVO.setMsg(msg);
        return  resultVO;
    }


    public  static ResultVO successMsg (String msg) {
        return success(null,msg);
    }


    public  static ResultVO success () {
        return success(null);
    }

    public  static ResultVO error (Integer code, String msg) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMsg(msg);
        return  resultVO;
    }


    public  static ResultVO error (String msg) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(error_code);
        resultVO.setMsg(msg);
        return  resultVO;
    }

}
