package com.zmj.reptiledemo.utils;

import com.zmj.reptiledemo.enums.CodeEnum;

public class EnumUtil {

    public static <T extends CodeEnum> String getByCode(Integer code, Class<T> t){
        for(T item: t.getEnumConstants()){
            if(item.getCode().equals(code)){
                return item.getText();
            }
        }
        return "";
    }
}
