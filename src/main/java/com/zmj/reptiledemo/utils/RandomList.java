package com.zmj.reptiledemo.utils;

import java.util.*;

public class RandomList {

    public static List randomN(List list, int n) {
        Map map = new HashMap();
        List listNew = new ArrayList();
        if (list.size() <= n) {
            return list;
        } else {
            while (map.size() < n) {
                int random = (int) (Math.random() * list.size());
                if (!map.containsKey(random)) {
                    map.put(random, "");
                    listNew.add(list.get(random));
                }
            }
            return listNew;
        }
    }

    public static List randomCollectionsN(List list, int n) {
        //shuffle 打乱顺序
        Collections.shuffle(list);
        List result = list.size() < n ? list : list.subList(0, n);
        return result;
    }


}
