package com.zmj.reptiledemo.aspect;

import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.exception.AuthorizeException;
import com.zmj.reptiledemo.exception.ResponseException;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Created by: meijun
 * Date: 2018/3/20 19:16
 */
@Aspect
@Component
@Slf4j
@Order(2)
public class BookReadAspect {

    @Autowired
    private StringRedisTemplate redisTemplate;


    @Pointcut("execution(public * com.zmj.reptiledemo.controller.BookController.bookinfo(..)) || " +
            "execution(public * com.zmj.reptiledemo.controller.BookController.search(..))  "
    )
    public void booRead() {
    }

    @Around("booRead()")
    public Object all( ProceedingJoinPoint joinPoint) throws Throwable{
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        BookUser user = getUser(request);

        Object[] args = joinPoint.getArgs();

        if (user != null) {
            String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
            for (int i = 0; i < argNames.length ; i++) {
                if (StringUtils.equals(argNames[i],"email")){
                    args[1] = user.getEmail();
                    break;
                }
            }
        }

        Object result =  joinPoint.proceed(args);
        return result;
    }

    private BookUser getUser(HttpServletRequest request) {

        String paramToken = request.getParameter(RedisConstant.COOKIE_TOKEN);

        Cookie cookie = CookieUtil.get(request, RedisConstant.COOKIE_TOKEN);
        String cookieToken = "";
        if (null != cookie) {
            cookieToken = cookie.getValue();
        }

        if(org.springframework.util.StringUtils.isEmpty(cookieToken) && org.springframework.util.StringUtils.isEmpty(paramToken)) {
            return null;
        }
        String token = org.springframework.util.StringUtils.isEmpty(paramToken)?cookieToken:paramToken;

        //查询redis
        String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX,token));
        BookUser bookUser = JSONObject.parseObject(tokenValue, BookUser.class);
        return bookUser;
    }

}
