package com.zmj.reptiledemo.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.exception.RequestLimitException;
import com.zmj.reptiledemo.interceptor.AccessLimit;
import com.zmj.reptiledemo.interceptor.UserContext;
import com.zmj.reptiledemo.mapper.ApiLogRepository;
import com.zmj.reptiledemo.redis.RedisService;
import com.zmj.reptiledemo.repository.ApiLog;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.utils.CookieUtil;
import com.zmj.reptiledemo.utils.IpUtil;
import com.zmj.reptiledemo.utils.KeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.javassist.*;
import org.apache.ibatis.javassist.bytecode.CodeAttribute;
import org.apache.ibatis.javassist.bytecode.LocalVariableAttribute;
import org.apache.ibatis.javassist.bytecode.MethodInfo;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/3/20 19:16
 */
@Aspect
@Component
@Slf4j
@Order(3)
public class ApiAspect {

    @Autowired
    private ApiLogRepository apiLogRepository;

    @Autowired
    private RedisService redisService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Pointcut("execution(public * com.zmj.reptiledemo.controller.*.*(..))")
    public void all() {
    }

    @Around("all()")
    public Object all( ProceedingJoinPoint joinPoint) throws Throwable{
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        HttpServletResponse response = attributes.getResponse();

        //接口限流
        BookUser user = getUser(request);
        UserContext.setUser(user);

        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        AccessLimit accessLimit = method.getAnnotation(AccessLimit.class);
        if(accessLimit != null) {
            int seconds = accessLimit.seconds();
            int maxCount = accessLimit.maxCount();
            boolean needLogin = accessLimit.needLogin();
            StringBuffer sb = new StringBuffer();
            String key = IpUtil.getIpAddr(request);
            if(needLogin) {
                if(user == null) {
                    render(response,ResultEnum.SESSION_ERROR);
                    return false;
                }
                sb.append(key).append("_").append(user.getEmail());
            }else {
                sb.append(key).append("_").append(method.getName());
            }

            Integer count = redisService.get( key, Integer.class);

            if(count == null) {
                redisService.set( sb.toString(), 1,seconds <= 0 ? 5:seconds);
            }else if(count < maxCount) {
                redisService.incr(0, sb.toString());
            }else {
//                render(response,ResultEnum.ACCESSLIMT);
                throw new RequestLimitException();
            }
        }


        //TODO 设置ip访问黑名单 防止别人攻击



        String classType = joinPoint.getTarget().getClass().getName();
        Class<?> clazz = Class.forName(classType);
        String clazzName = clazz.getName();
        String methodName = joinPoint.getSignature().getName(); //获取方法名称


        Object[] args = joinPoint.getArgs();//参数
        Map<String, Object> map = getFieldsName(this.getClass(), clazzName, methodName, args);

        map.remove("request");
        map.remove("model");
        map.remove("this");
        map.remove("reponse");
        map.remove("md5Str");

        //记录起始时间
        long begin = System.currentTimeMillis();
        Object result = "";
        /** 执行目标方法 */
        result= joinPoint.proceed();
        /** 记录操作时间 */
        long took = (System.currentTimeMillis() - begin);

        ApiLog apiLog = new ApiLog();
        apiLog.setId(KeyUtil.genUniqueKey());
        apiLog.setController(clazzName);
        apiLog.setMethod(methodName);
        apiLog.setIp(IpUtil.getIpAddr(request));

        //查询cookie
        Cookie cookie = CookieUtil.get(request, RedisConstant.COOKIE_TOKEN);
        if(null !=  cookie){
            //查询redis
            String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX,cookie.getValue()));
            if(StringUtils.isNotBlank(tokenValue)){
                BookUser bookUser = JSONObject.parseObject(tokenValue, BookUser.class);
                apiLog.setUser(bookUser.getEmail());
            }
        }
        apiLog.setParams(map.toString());
        apiLog.setExecute(took);
        apiLog.setRequestTime(new Date());

        apiLogRepository.save(apiLog);

        return result;
    }

    /**
     * 通过反射机制 获取被切参数名以及参数值
     *
     * @param cls
     * @param clazzName
     * @param methodName
     * @param args
     * @return
     * @throws NotFoundException
     */
    private Map<String, Object> getFieldsName(Class cls, String clazzName, String methodName, Object[] args) throws NotFoundException {
        Map<String, Object> map = new HashMap<String, Object>();

        ClassPool pool = ClassPool.getDefault();
        //ClassClassPath classPath = new ClassClassPath(this.getClass());
        ClassClassPath classPath = new ClassClassPath(cls);
        pool.insertClassPath(classPath);

        CtClass cc = pool.get(clazzName);
        CtMethod cm = cc.getDeclaredMethod(methodName);
        MethodInfo methodInfo = cm.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
        if (attr == null) {
            // exception
        }
        // String[] paramNames = new String[cm.getParameterTypes().length];
        int pos = Modifier.isStatic(cm.getModifiers()) ? 0 : 1;
        for (int i = 0; i < cm.getParameterTypes().length; i++) {
            map.put(attr.variableName(i + pos), args[i]);//paramNames即参数名
        }
        return map;
    }

    private void render(HttpServletResponse response, ResultEnum resultEnum) throws Exception {
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();

        String str = JSON.toJSONString(resultEnum.getMsg());
        outputStream.write(str.getBytes("UTF-8"));
        outputStream.flush();
        outputStream.close();
    }


    private BookUser getUser(HttpServletRequest request) {
        String paramToken = request.getParameter(RedisConstant.COOKIE_TOKEN);

        String cookieToken = getCookieValue(request, RedisConstant.COOKIE_TOKEN);

        if(org.springframework.util.StringUtils.isEmpty(cookieToken) && org.springframework.util.StringUtils.isEmpty(paramToken)) {
            return null;
        }
        String token = org.springframework.util.StringUtils.isEmpty(paramToken)?cookieToken:paramToken;

        //查询redis
        String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX,token));
        BookUser bookUser = JSONObject.parseObject(tokenValue, BookUser.class);
        return bookUser;
    }

    private String getCookieValue(HttpServletRequest request, String cookiName) {
        Cookie[] cookies = request.getCookies();
        if(null == cookies || cookies.length <= 0) {
            return null;
        }
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals(cookiName)) {
                return cookie.getValue();
            }
        }
        return null;
    }
}
