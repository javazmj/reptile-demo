package com.zmj.reptiledemo.aspect;

import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.enums.ResultEnum;
import com.zmj.reptiledemo.exception.AuthorizeException;
import com.zmj.reptiledemo.exception.ResponseException;
import com.zmj.reptiledemo.repository.BookUser;
import com.zmj.reptiledemo.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by: meijun
 * Date: 2018/3/20 19:16
 */
@Aspect
@Component
@Slf4j
@Order(1)
public class LoginAspect {

    @Autowired
    private StringRedisTemplate redisTemplate;


    @Pointcut("execution(public * com.zmj.reptiledemo.controller.LoginController.my(..)) || " +
            "execution(public * com.zmj.reptiledemo.controller.LoginController.addShelf(..)) || " +
            "execution(public * com.zmj.reptiledemo.controller.LoginController.under(..))"
    )
    public void myAuthorize() {
    }

    @Around("myAuthorize()")
    public Object all( ProceedingJoinPoint joinPoint) throws Throwable{
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        HttpServletResponse response = attributes.getResponse();
            //查询cookie

        Cookie cookie = CookieUtil.get(request, RedisConstant.COOKIE_TOKEN);

        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        ResponseBody responseBody = method.getAnnotation(ResponseBody.class);
        if(null == cookie){
            log.warn("[登录校验] , cookie 中查不到token");
            if (responseBody == null) {
                throw new AuthorizeException(ResultEnum.NOT_LOGIN);
            }else {
                throw new ResponseException(ResultEnum.NOT_LOGIN);
            }
        }
        //查询redis
        String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX,cookie.getValue()));
        if(StringUtils.isEmpty(tokenValue)){
            log.warn("[登录校验] Redis中查不到token");
            if (responseBody == null) {
                throw new AuthorizeException(ResultEnum.NOT_LOGIN);
            }else {
                throw new ResponseException(ResultEnum.NOT_LOGIN);
            }
        }
        BookUser bookUser = JSONObject.parseObject(tokenValue, BookUser.class);
        Object[] args = joinPoint.getArgs();
        String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
        for (int i = 0; i < argNames.length ; i++) {
            if (StringUtils.equals(argNames[i],"email")){
                args[1] = bookUser.getEmail();
            }
        }
        Object result =  joinPoint.proceed(args);
        return result;
    }

}
