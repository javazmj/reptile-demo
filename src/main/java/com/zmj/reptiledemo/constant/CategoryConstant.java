package com.zmj.reptiledemo.constant;

public class CategoryConstant{

    public static final String xh = "玄幻小说";
    public static final String xz = "修真小说";
    public static final String ds = "都市小说";
    public static final String ls = "历史小说";
    public static final String wy = "网游小说";
    public static final String kh = "科幻小说";
    public static final String np = "女生频道";
    public static final String ph = "排行榜单";
    public static final String wb = "完成";

}
