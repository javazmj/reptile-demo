package com.zmj.reptiledemo.constant;

import java.util.ArrayList;
import java.util.List;

public class BaseLinks {

    public static final String url = "https://www.biqudu.net";

    public static final String searchUrl = "https://www.biqudu.net/searchbook.php?keyword=";

    /**
     * 玄幻小说
     */
    public static final String xh = url +  "/xuanhuanxiaoshuo/";
    /**
     * 修真小说
     */
    public static final String xz = url +  "/xiuzhenxiaoshuo/";
    /**
     * 都市小说
     */
    public static final String ds = url +   "/dushixiaoshuo/";
    /**
     * 历史小说
     */
    public static final String ls = url +   "/lishixiaoshuo/";
    /**
     * 网游小说
     */
    public static final String wy = url +  "/wangyouxiaoshuo/";
    /**
     * 科幻小说
     */
    public static final String kh = url +  "/kehuanxiaoshuo/";
    /**
     * 女频小说
     */
    public static final String np = url +  "/nvpinxiaoshuo/";
    /**
     * 排行榜单
     */
    public static final String phb = url +  "/paihangbang/";
    /**
     * 完本小说
     */
    public static final String wb = url +  "/wanbenxiaoshuo/";

    public static final List<String> list = new ArrayList<>();


    static {
        list.add(xh);
        list.add(xz);
        list.add(ds);
        list.add(ls);
        list.add(wy);
        list.add(kh);
        list.add(np);
        list.add(phb);
        list.add(wb);
    }

    /**
     * 已经爬取的小说名urls
     */
    public static final String WIN_LINK_REDIS_PREFIX = "win_link_urls";

    /**
     * 用户实时搜索需要爬取的小说列表
     *
     * 该列表要求实时性较高 所以设置job任务时间间隔很短
     * 和平时的区别
     */
    public static final String SEARCE_LINK_REDIS_PREFIX = "search_link_urls";

    public static final int EDIS_BOOKLIST_CACHE = 3;

    public static final String REDIS_BOOKLIST = "booklist";

    public static final int EDIS_BOOKLIST_EXPIRE = 60 * 60 * 2;

    public static final int TIMEOUT = 20000;
}
