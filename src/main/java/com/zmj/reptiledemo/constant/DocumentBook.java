package com.zmj.reptiledemo.constant;

public class DocumentBook {

    public static final String description = "og:description";
    public static final String image = "og:image";
    public static final String category = "og:novel:category";
    public static final String author = "og:novel:author";
    public static final String book_name = "og:novel:book_name";
    public static final String read_url = "og:novel:read_url";
    public static final String status = "og:novel:status";
    public static final String update_time = "og:novel:update_time";
    public static final String latest_chapter_name = "og:novel:latest_chapter_name";
    public static final String latest_chapter_url = "og:novel:latest_chapter_url";
}
