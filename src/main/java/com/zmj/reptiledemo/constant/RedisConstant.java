package com.zmj.reptiledemo.constant;

public class RedisConstant {


    public static final String TOKEN_PREFIX = "token_%s";

    public static final Integer EXPIRE = 60 * 60 * 24 * 7;


    public static final String COOKIE_TOKEN = "token";

    public static final String COOKIE_ACCESS_TOKEN= "access_token";

    public static final int  BOOK_INFO_CACHE_INDEX = 6;

    public static final int BOOK_INFO_CACHE_EXPIRE = 60 * 60 * 2;

}
