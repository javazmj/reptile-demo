package com.zmj.reptiledemo.redis;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class RedisService {

    @Autowired
    private JedisPool jedisPool;

    /**
     * 选择索引位置
     * @param jedis
     * @param index
     * @return
     */
    public void selectIndex(Jedis jedis,int index) {
        jedis.select(index);
    }


    public Long setnx(int index,String key ,String value){
        Jedis jedis =null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.setnx(key,value);
        }catch (Exception e){
            log.error("expire key:{} error",key,e);
            jedisPool.returnResource(jedis);
            return  result;
        }
        jedisPool.returnResource(jedis);
        return  result;

    }

    public String setex(int index,String key,int expire,String value){
        Jedis jedis =null;
        String result = null;
        try {
            jedis = jedisPool.getResource();
            jedis.select(index);
            result = jedis.setex(key,expire,value);
        }catch (Exception e){
            log.error("expire key:{} error",key,e);
            jedisPool.returnResource(jedis);
            return  result;
        }
        jedisPool.returnResource(jedis);
        return  result;

    }

    public Set<String> smembers(int index, String key){
        Jedis jedis =null;
        Set<String> result = null;
        try {
            jedis = jedisPool.getResource();
            jedis.select(index);
            result = jedis.smembers(key);
        }catch (Exception e){
            log.error("smembers key:{} error",key,e);
            jedisPool.returnResource(jedis);
            return  result;
        }
        jedisPool.returnResource(jedis);
        return  result;

    }

    /**
     * 设置key的有效期，单位是秒
     * @param key
     * @param exTime
     * @return
     */
    public Long expire(int index,String key,int exTime){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }

    public  String get(int index,String key){
        Jedis jedis = null;
        String result = null;
        try {
            jedis =  jedisPool.getResource();
            jedis.select(index);
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }

    public <T> T get(String key,Class<T> clazz) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            String str = jedis.get(key);
            T t = stringToBean(str,clazz);
            return  t;
        }finally {
            returnToPool(jedis);
        }
    }


    public  String getset(int index,String key,String value){
        Jedis jedis = null;
        String result = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.getSet(key,value);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }

    /**
     * 设置对象
     * */
    public <T> boolean set(int index,String key, T value,int seconds) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            String str = beanToString(value);
            if(str == null || str.length() <= 0) {
                return false;
            }
            if(seconds <= 0) {
                jedis.set(key, str);
            }else {
                jedis.setex(key, seconds, str);
            }
            return true;
        }finally {
            returnToPool(jedis);
        }
    }
    public <T> boolean set(String key, T value,int seconds) {
        return set(0, key,  value, seconds);
    }



    /**
     * 判断key是否存在
     * */
    public <T> boolean exists(int index,String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            return  jedis.exists(key);
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 删除
     * */
    public boolean delete(int index,String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            long ret =  jedis.del(key);
            return ret > 0;
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 增加值
     * */
    public <T> Long incr(int index,String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            return  jedis.incr(key);
        }finally {
            returnToPool(jedis);
        }
    }


    public  Long del(int index,String key){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error",key,e);
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }


    public  Long sadd(int index,String key,String...value){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.sadd(key,value);
        } catch (Exception e) {
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }

    public List<String> srandmember(int index,String key,int n){
        Jedis jedis = null;
        List<String> result = null;
        try {
            jedis =  jedisPool.getResource();
            selectIndex( jedis, index);
            result = jedis.srandmember(key,n);
        } catch (Exception e) {
            jedisPool.returnBrokenResource(jedis);
            return result;
        }
        jedisPool.returnResource(jedis);
        return result;
    }


    public List<String> scanKeys(int index,String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            List<String> keys = new ArrayList<String>();
            String cursor = "0";
            ScanParams sp = new ScanParams();
            sp.match("*"+key+"*");
            sp.count(100);
            do{
                ScanResult<String> ret = jedis.scan(cursor, sp);
                List<String> result = ret.getResult();
                if(result!=null && result.size() > 0){
                    keys.addAll(result);
                }
                //再处理cursor
                cursor = ret.getStringCursor();
            }while(!cursor.equals("0"));
            return keys;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static <T> String beanToString(T value) {
        if(value == null) {
            return null;
        }
        Class<?> clazz = value.getClass();
        if(clazz == int.class || clazz == Integer.class) {
            return ""+value;
        }else if(clazz == String.class) {
            return (String)value;
        }else if(clazz == long.class || clazz == Long.class) {
            return ""+value;
        }else {
            return JSON.toJSONString(value);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T stringToBean(String str, Class<T> clazz) {
        if(str == null || str.length() <= 0 || clazz == null) {
            return null;
        }
        if(clazz == int.class || clazz == Integer.class) {
            return (T)Integer.valueOf(str);
        }else if(clazz == String.class) {
            return (T)str;
        }else if(clazz == long.class || clazz == Long.class) {
            return  (T)Long.valueOf(str);
        }else {
            return JSON.toJavaObject(JSON.parseObject(str), clazz);
        }
    }

    private void returnToPool(Jedis jedis) {
        if(jedis != null) {
            jedis.close();
        }
    }
}
