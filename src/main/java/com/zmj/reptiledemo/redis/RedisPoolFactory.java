package com.zmj.reptiledemo.redis;

import com.zmj.reptiledemo.config.RedisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Service
public class RedisPoolFactory {

    @Autowired
    private RedisConfig redisConfig;

    @Bean
    public JedisPool jedisPoolFactory(){
      /*  redisConfig jpc = new redisConfig();
        jpc.setMaxTotal(redisConfig.getPoolMaxActive());
        JedisPool jp = new JedisPool(jpc,
                redisConfig.getHost(),
                redisConfig.getPort(),
                redisConfig.getTimeout(),
                redisConfig.getPassword(),
                0);
        return jp;*/

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认true
        jedisPoolConfig.setBlockWhenExhausted(true);
        // 是否启用pool的jmx管理功能, 默认true
        jedisPoolConfig.setJmxEnabled(true);
        // 最大连接数, 默认8个
        jedisPoolConfig.setMaxTotal(redisConfig.getPoolMaxActive());
        // 最大空闲连接数, 默认8个 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例
        jedisPoolConfig.setMaxIdle(redisConfig.getPoolMaxIdle());
        // 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
        jedisPoolConfig.setMaxWaitMillis(redisConfig.getMaxWait() * 1000);
        // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
        jedisPoolConfig.setTestOnBorrow(true);

        JedisPool jp = new JedisPool(jedisPoolConfig,
                redisConfig.getHost(),redisConfig.getPort(),
                redisConfig.getTimeout(),
                redisConfig.getPassword());
        return jp;
    }
}
