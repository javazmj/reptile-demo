package com.zmj.reptiledemo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Http请求返回的对象
 * Created by: meijun
 * Date: 2017/12/19 13:16
 */
@Data
public class ResultVO<T> implements Serializable {

    private static final long serialVersionUID = -1896396588821960462L;
    //错误码
    private Integer code;
    //提示消息
    private String  msg;
    //返回内容
    private T data;
}
