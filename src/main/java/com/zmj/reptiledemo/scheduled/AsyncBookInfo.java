package com.zmj.reptiledemo.scheduled;

import com.alibaba.fastjson.JSONObject;
import com.zmj.reptiledemo.constant.RedisConstant;
import com.zmj.reptiledemo.redis.RedisService;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.service.EsBookInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AsyncBookInfo {

    @Autowired
    private RedisService redisService;

    @Autowired
    private EsBookInfoService esbookInfoService;


    @Async
    public void info(String bookId,String readUrl) {
        if (!redisService.exists(RedisConstant.BOOK_INFO_CACHE_INDEX,readUrl)) {
            List<BookInfo> bookInfos = esbookInfoService.getByBookIdAndSort(bookId,readUrl);
            int index = 0;
            if (bookInfos != null && bookInfos.size() > 0) {
                BookInfo bookInfo =  setBookInfoRedis( bookId, bookInfos.get(0).getNext());
                while (bookInfo != null && index < 19) {
                    redisService.setex(RedisConstant.BOOK_INFO_CACHE_INDEX,
                            bookInfo.getReadUrl(),RedisConstant.BOOK_INFO_CACHE_EXPIRE, JSONObject.toJSONString(bookInfo));
                    bookInfo = setBookInfoRedis(bookId,bookInfo.getNext());
                    index += 1;
                }
            }

        }
    }

    private  BookInfo setBookInfoRedis(String bookId,String readurl) {
        List<BookInfo> sort = esbookInfoService.getByBookIdAndSort(bookId, readurl);
        if (sort != null && sort.size() > 0) {
            return sort.get(0);
        }
        return  null;
    }
}
