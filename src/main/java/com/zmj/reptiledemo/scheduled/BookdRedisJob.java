package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.service.EsBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;


@Component
@Slf4j
public class BookdRedisJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private EsBookService esBookService;

    /**
     * 每隔两小时从es同步到redis一次小说名
     */
//    @Scheduled(cron = "* * 1/2 * * ? ")
    public void bookJob() {
        List<Book> list = esBookService.queryAll();
        Set<String> members = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);

        list.forEach(b -> {
            String id = "/" + b.getId() + "/";
            if (!members.contains(id)) {
                redisTemplate.opsForSet().add(BaseLinks.WIN_LINK_REDIS_PREFIX,id);
                log.info("设置book的id到redis  win_link_urls: {}",id);
            }
        });
    }

}
