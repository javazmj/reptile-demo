package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.document.Doc2Bean;
import com.zmj.reptiledemo.redis.RedisService;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.service.EsBookInfoService;
import com.zmj.reptiledemo.utils.http.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class JobBookInfoThread {

    @Autowired
    @Qualifier("jobExecutor")
    private Executor execute;

    @Autowired
    private EsBookInfoService esBookInfoService;

    @Autowired
    private RedisService redisService;


    public void job(String path,String id) {
        this.execute.execute(new JobBookInfoWorkThread(path,id));
    }

    private class JobBookInfoWorkThread implements Runnable {
        private String path;
        private String id;

        private JobBookInfoWorkThread(String path,String id) {
            super();
            this.path = path;
            this.id = id;
        }

        @Override
        public void run() {
            try {
                Document document = HttpClientUtil.getRequest(BaseLinks.url + path);

                BookInfo bookInfo = new BookInfo();
                Doc2Bean.Doc2BeanBookInfo(document,bookInfo);
                bookInfo.setBookId(id);
                bookInfo.setReadUrl(path);

                int byId = esBookInfoService.getById(path);

                if (byId == 0) {
                    esBookInfoService.addBookInfo(bookInfo,false);
                    log.info("成功入库小说 {} 正文第: {} 章",id,bookInfo.getReadUrl());
                }
            } catch (Exception e) {
                log.error("HttpClientUtils. JobBookInfoThread 连接失败");
            }finally {
                // 无论是否poll到数据，均暂停一小段时间，可降低CPU消耗
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
