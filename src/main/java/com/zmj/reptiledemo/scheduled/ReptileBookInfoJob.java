package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.service.EsBookInfoService;
import com.zmj.reptiledemo.service.EsBookListService;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Component
@Slf4j
public class ReptileBookInfoJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private EsBookListService esBookListService;

    @Autowired
    private EsBookInfoService esBookInfoService;

    @Autowired
    private JobBookInfoThread jobBookInfoThread;

    /**
     * 平时需要更新的列表设置为每天凌晨一点更新
     */
    @Scheduled(cron = "0 0 1 * * ? ")
    public void bookInfoJob() {
        try {
            //已爬取的小说名字列表
            if (redisTemplate.hasKey(BaseLinks.WIN_LINK_REDIS_PREFIX)) {
                Set<String> winMembers = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);
                if (winMembers != null && winMembers.size() > 0) {
                    Iterator<String> iterator = winMembers.iterator();
                    while (iterator.hasNext()) {
                        String bookId = iterator.next();
                        List<BookInfo> infoList = esBookInfoService.getByBookId(UrlUtils.getTrimId(bookId));
                        Set<String> readUrlSet = infoList.stream().map(b -> b.getReadUrl()).collect(Collectors.toSet());

                        try {
                            List<BookList> bookLists = esBookListService.getByBookIdAndSort(UrlUtils.getTrimId(bookId),0);
                            if (bookLists != null && bookLists.size() > 0) {
                                for (BookList bk: bookLists) {
                                    if (readUrlSet != null & readUrlSet.size() > 0) {
                                        //如果不包含这个章节 说明正文没入库
                                        if (!readUrlSet.contains(bk.getReadUrl())) {
                                            jobBookInfoThread.job(bk.getReadUrl(), UrlUtils.getTrimId(bookId));
                                        }
                                    }else {
                                        //说明正文一章都没有
                                        jobBookInfoThread.job(bk.getReadUrl(), UrlUtils.getTrimId(bookId));
                                    }
                                }
                                if (readUrlSet.size() == bookLists.size()) {
                                    //入库后删除用户搜索的 需要爬取的缓存
                                    redisTemplate.opsForSet().remove(BaseLinks.SEARCE_LINK_REDIS_PREFIX,bookId);
                                    log.info("{}小说更新章节已全部入库",bookId);
                                }
                            }
                        }catch (NoNodeAvailableException exception) {
                            List<BookList> bookLists = esBookListService.getByBookIdAndSort(UrlUtils.getTrimId(bookId),0);
                            if (bookLists != null && bookLists.size() > 0) {
                                for (BookList bk: bookLists) {
                                    if (readUrlSet != null & readUrlSet.size() > 0) {
                                        //如果不包含这个章节 说明正文没入库
                                        if (!readUrlSet.contains(bk.getReadUrl())) {
                                            jobBookInfoThread.job(bk.getReadUrl(), UrlUtils.getTrimId(bookId));
                                        }
                                    }else {
                                        //说明正文一章都没有
                                        jobBookInfoThread.job(bk.getReadUrl(), UrlUtils.getTrimId(bookId));
                                    }
                                }
                                if (readUrlSet.size() == bookLists.size()) {
                                    //入库后删除用户搜索的 需要爬取的缓存
                                    redisTemplate.opsForSet().remove(BaseLinks.SEARCE_LINK_REDIS_PREFIX,bookId);
                                    log.info("{}小说更新章节已全部入库",bookId);
                                }
                            }
                        }
                    }
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
