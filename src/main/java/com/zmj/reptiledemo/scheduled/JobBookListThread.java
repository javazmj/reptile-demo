package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.service.EsBookListService;
import com.zmj.reptiledemo.service.EsBookService;
import com.zmj.reptiledemo.document.Doc2Bean;
import com.zmj.reptiledemo.utils.http.HttpClientUtil;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Component
@Slf4j
public class JobBookListThread {

    @Autowired
    @Qualifier("jobExecutor")
    private Executor execute;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private EsBookService esBookService;

    @Autowired
    private EsBookListService esBookListService;


    public void job(String path,String id) {
        this.execute.execute(new JobBookListWorkThread(path,id));
    }

    private class JobBookListWorkThread implements Runnable {
        private String path;
        private String id;

        private JobBookListWorkThread(String path,String id) {
            super();
            this.path = path;
            this.id = id;
        }

        @Override
        public void run() {
            try {
                Document document = HttpClientUtil.getRequest(path);
                Thread.sleep(100);
                Book book = new Book();
                String trimId = UrlUtils.getTrimId(id);
                book.setId(trimId);
                Doc2Bean.Doc2BeanBook(document,book);

                List<BookList> list = new ArrayList<>();

                //获取到已经入库的小说章节列表
                List<BookList> bookLists = esBookListService.getByBookIdAndSort(trimId,0);
                Set<String> readUrlSet = null;

                int save = esBookService.getById(trimId);
                //已经数据库已经存在的话 更新最新章节 并更新小说最新章节链接名
                if (bookLists != null && bookLists.size() > 0) {
                    readUrlSet = bookLists.stream().map(b -> b.getReadUrl()).collect(Collectors.toSet());
                    //更新book里的最新章节链接和名称
                    Book updateBook = new Book();
                   if (save == 1) {
                       updateBook.setId(book.getId());
                       updateBook.setLatestChapterName(book.getLatestChapterName());
                       updateBook.setLatestChapterUrl(book.getLatestChapterUrl());
                       esBookService.addBook(book);
                   }
                }
                if (save == 0) {
                    //入库小说名
                    esBookService.addBook(book);
                    //将入库的小说名缓存
                    redisTemplate.opsForSet().add(BaseLinks.WIN_LINK_REDIS_PREFIX,id);
                    log.info("成功入库小说: {} ",book.getBookName());
                }
                Doc2Bean.Doc2BeanBookList(document,list,trimId,readUrlSet);
                //入库章节列表
                esBookListService.batchAddBookList(list);
                log.info("成功入库目录: {} ,共{} 章",book.getBookName(),list.size());

            } catch (Exception e) {
                e.printStackTrace();
                log.error("httpClientResult JobBookListThread 连接超时");
            }

        }
    }
}
