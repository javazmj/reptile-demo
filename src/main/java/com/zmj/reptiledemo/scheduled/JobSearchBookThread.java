package com.zmj.reptiledemo.scheduled;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.document.Doc2Bean;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.service.EsBookListService;
import com.zmj.reptiledemo.utils.http.HttpClientUtil;
import com.zmj.reptiledemo.utils.http.HttpClientUtils;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Component
@Slf4j
public class JobSearchBookThread {

    @Autowired
    @Qualifier("jobExecutor")
    private Executor execute;

    @Autowired
    private EsBookListService esBookListService;


    public void job(String path) {
        this.execute.execute(new JobWorkThread(path));
    }

    private class JobWorkThread implements Runnable {
        private String path;

        private JobWorkThread(String path) {
            super();
            this.path = path;
        }

        @Override
        public void run() {
            try {
                Document document = HttpClientUtil.getRequest(BaseLinks.url + path);
                Book book = new Book();
                String trimId = UrlUtils.getTrimId(path);
                book.setId(trimId);
                Doc2Bean.Doc2BeanBook(document,book);
                //获取小说名
//            String book_name = book.getElementsByAttributeValue("property", "og:novel:book_name").
//                    get(0).attr("content");

                List<BookList> list = new ArrayList<>();

                //获取到已经入库的小说章节列表
                List<BookList> bookLists = esBookListService.getByBookIdAndSort(trimId,0);

                if (bookLists != null && bookLists.size() > 0) {
                    Set<String> readUrlSet = bookLists.stream().map(b -> b.getReadUrl()).collect(Collectors.toSet());
                    Doc2Bean.Doc2BeanBookList(document,list,trimId,readUrlSet);

                }

                List<BookInfo> infoList = Lists.newArrayList();
                for (BookList bookList:list) {
                    Document info = HttpClientUtils.reptile(BaseLinks.url + bookList.getReadUrl());
                    BookInfo bookInfo = new BookInfo();
                    Doc2Bean.Doc2BeanBookInfo(info,bookInfo);
                    bookInfo.setBookId(trimId);
                    bookInfo.setReadUrl(bookList.getReadUrl());
                    infoList.add(bookInfo);
                }


            } catch (Exception e) {
                log.error("httpClientResult JobSearchBookThread连接超时");
            }

        }
    }
}
