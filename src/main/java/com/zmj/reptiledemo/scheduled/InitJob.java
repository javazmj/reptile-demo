package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.utils.http.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Set;


@Component
@Slf4j
public class InitJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private JobBookListThread jobBookListThread;

    /**
     * 初始化需要爬取的url
     */
//    @Scheduled(cron = "0/10 * * * * ? ")
    public void bookJob() {
        try {
            Set<String> members = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);
            if (members == null || members.size() == 0) {
                for (String url: BaseLinks.list) {
                    Document document = HttpClientUtils.reptile(url);
                    Elements elements = document.select("span[class=s2]");
                    for (Element e :elements) {
                        String link = e.select("a").attr("href");
                        Set<String> winMembers = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);
                        if (!winMembers.contains(link)) {
                            String bookurl = BaseLinks.url + link;
                            jobBookListThread.job(bookurl,link);
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
