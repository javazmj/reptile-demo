package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Set;


@Component
@Slf4j
public class ReptileUpdateBookJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private JobBookListThread jobBookListThread;

    /**
     * 用户实时搜索需要爬取的小说
     * 间隔时间较短 为了快速入库
     */
    @Scheduled(cron = "5/20 * * * * ? ")
    public void bookJob() {
        try {
            if (redisTemplate.hasKey(BaseLinks.SEARCE_LINK_REDIS_PREFIX)) {
                Set<String> members = redisTemplate.opsForSet().members(BaseLinks.SEARCE_LINK_REDIS_PREFIX);
                if (members != null && members.size() > 0) {
                    Iterator<String> iterator = members.iterator();
                    while (iterator.hasNext()) {
                        String bookId = iterator.next();
                        String url = BaseLinks.url + bookId;
                        jobBookListThread.job(url,bookId);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
