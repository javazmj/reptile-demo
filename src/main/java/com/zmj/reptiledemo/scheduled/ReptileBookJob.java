package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Set;


@Component
@Slf4j
public class ReptileBookJob {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private JobBookListThread jobBookListThread;

    /**
     * 初始化需要爬取的url
     */
//    @Scheduled(cron = "0/20 * * * * ? ")
    public void bookJob() {
        try {
            if (redisTemplate.hasKey(BaseLinks.WIN_LINK_REDIS_PREFIX)) {
                Set<String> members = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);
                if (members != null && members.size() > 0) {
                    Iterator<String> iterator = members.iterator();
                    while (iterator.hasNext()) {
                        String bookId = iterator.next();
                        String url = BaseLinks.url + bookId;
                        jobBookListThread.job(url,bookId);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
