package com.zmj.reptiledemo.scheduled;

import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.utils.http.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.Executor;

@Component
@Slf4j
public class JobThread {

    @Autowired
    @Qualifier("jobExecutor")
    private Executor execute;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private JobBookListThread jobBookListThread;


    public void job(String path) {
        this.execute.execute(new JobWorkThread(path));
    }

    private class JobWorkThread implements Runnable {
        private String path;

        private JobWorkThread(String path) {
            super();
            this.path = path;
        }

        @Override
        public void run() {
            try {
                Document document = HttpClientUtil.getRequest(path);
                Elements elements = document.select("span[class=s2]");
                for (Element e :elements) {
                    String link = e.select("a").attr("href");
                    Set<String> winMembers = redisTemplate.opsForSet().members(BaseLinks.WIN_LINK_REDIS_PREFIX);
                    if (!winMembers.contains(link)) {
                        String url = BaseLinks.url + link;
                        jobBookListThread.job(url,link);
                    }
                }
            } catch (Exception e) {
                log.error("初始化爬取小说列表httpClientResult连接超时");
            }

        }
    }
}
