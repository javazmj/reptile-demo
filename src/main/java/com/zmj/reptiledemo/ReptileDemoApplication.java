package com.zmj.reptiledemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;


//@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class })
@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableElasticsearchRepositories(basePackages = "com.zmj.reptiledemo.mapper")
@CrossOrigin(maxAge = 3600)
public class ReptileDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReptileDemoApplication.class, args);
    }

}
