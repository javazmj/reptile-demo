package com.zmj.reptiledemo.init;

import com.zmj.reptiledemo.scheduled.JobBookInfoThread;
import com.zmj.reptiledemo.scheduled.JobThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MyCommandRunner implements CommandLineRunner {

    @Autowired
    private JobThread jobThread;
    @Autowired
    private JobBookInfoThread jobBookInfoThread;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void run(String... args) throws Exception {
      /*  Set<String> members = redisTemplate.opsForSet().members(BaseLinks.WAIT_LINK_REDIS_PREFIX);
        if (members == null || members.size() == 0) {
            for (String url: BaseLinks.list) {
                jobThread.job(url);
            }
        }*/
//        jobThread.job("https://www.biqudu.net/0_1/");
    }
}
