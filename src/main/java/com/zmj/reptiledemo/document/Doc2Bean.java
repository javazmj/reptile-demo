package com.zmj.reptiledemo.document;

import com.zmj.reptiledemo.constant.DocumentBook;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.util.*;

public class Doc2Bean {

    private static final String pattern = "yyyy-MM-dd HH:mm:ss";
    private static final String DOMAIN = "http://book.zmjmall.com/";
    private static final String property = "property";
    private static final String content = "content";

    public static void Doc2BeanBook(Document document, Book book) {
        String author = document.getElementsByAttributeValue(property, DocumentBook.author).
                get(0).attr(content);
        String book_name = document.getElementsByAttributeValue(property, DocumentBook.book_name).
                get(0).attr(content);
        String category = document.getElementsByAttributeValue(property, DocumentBook.category).
                get(0).attr(content);
        String description = document.getElementsByAttributeValue(property, DocumentBook.description).
                get(0).attr(content);
        String image = document.getElementsByAttributeValue(property, DocumentBook.image).
                get(0).attr(content);
        String latest_chapter_name = document.getElementsByAttributeValue(property, DocumentBook.latest_chapter_name).
                get(0).attr(content);
        String latest_chapter_url = document.getElementsByAttributeValue(property, DocumentBook.latest_chapter_url).
                get(0).attr(content);
        String read_url = document.getElementsByAttributeValue(property, DocumentBook.read_url).
                get(0).attr(content);
        String status = document.getElementsByAttributeValue(property, DocumentBook.status).
                get(0).attr(content);
        String update_time = document.getElementsByAttributeValue(property, DocumentBook.update_time).
                get(0).attr(content);


        book.setAuthor(author);
        book.setBookName(book_name);
        book.setCategory(category);
        book.setDescription(description);
        book.setImage(image);
        book.setLatestChapterName(latest_chapter_name);
        book.setLatestChapterUrl(UrlUtils.getSort(latest_chapter_url));
        book.setReadUrl(UrlUtils.getTrimId(read_url));
        book.setStatus(status);
        book.setCreateTime(new Date());
        try {
            book.setUpdateTime(DateUtils.parseDate(update_time,pattern));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }


    public static void Doc2BeanBookList(Document document, List<BookList> bookList, String bookId,Set<String> readSet){
        Element list = document.getElementById("list");
        Elements dds = list.select("dd").select("a");
        Set<Map<String,String>> objects = new HashSet<>();
        for (Element e :dds ) {
            Map<String,String> urlMap = new HashMap<>();
            String href = e.attr("href");
            String sort = UrlUtils.getSort(href);
            if (readSet != null) {
                if (!readSet.contains(href)) {
                    urlMap.put("sort",sort);
                    urlMap.put("href",href);
                    urlMap.put("text",e.text());
                    objects.add(urlMap);
                    readSet.add(href);
                }

            }else {
                urlMap.put("sort",sort);
                urlMap.put("href",href);
                urlMap.put("text",e.text());
                objects.add(urlMap);
            }
        }
        Iterator<Map<String, String>> iterator = objects.iterator();
        while (iterator.hasNext()) {
            Map<String, String> map = iterator.next();
            BookList bkl = new BookList();
            bkl.setBookId(bookId);
            bkl.setName(map.get("text" ));
            String href = map.get("href");
            bkl.setReadUrl(href);
            bkl.setSort(Integer.parseInt(String.valueOf(map.get("sort"))));
            bookList.add(bkl);
        }
    }

    public static void Doc2BeanBookInfo(Document document, BookInfo bookInfo) {

        String preHref = document.getElementsByAttributeValue("class", "pre").get(0).attr("href");
        String nextHref = document.getElementsByAttributeValue("class", "next").get(0).attr("href");
        String content = document.getElementById("content").toString();
        bookInfo.setPre(UrlUtils.getSort(preHref));
        bookInfo.setNext(UrlUtils.getSort(nextHref));
        bookInfo.setContent(content);
    }
}
