package com.zmj.reptiledemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.data.elasticsearch")
@Data
public class ElasticSearchConfig {

    private String clusterNodes;

    private String clusterName;

}
