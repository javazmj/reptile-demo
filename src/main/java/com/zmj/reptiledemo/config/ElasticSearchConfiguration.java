package com.zmj.reptiledemo.config;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.net.InetAddress;

@Configuration
@Slf4j
public class ElasticSearchConfiguration {

    @Autowired
    private ElasticSearchConfig esConfig;

    @PostConstruct
    void init() {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
    }

    @Bean
    public TransportClient transportClient() {
        log.info("ElasticSearch TransportClient初始化...");
        TransportClient client = null;
        try {
            TransportAddress transportAddress = new InetSocketTransportAddress(InetAddress.getByName(esConfig.getClusterNodes()),
                    9300);

            // 配置信息
            Settings esSetting = Settings.builder()
                    .put("cluster.name",esConfig.getClusterName())
                    .build();

            //配置信息Settings自定义
            client = new PreBuiltTransportClient(esSetting);

            client.addTransportAddresses(transportAddress);

        } catch (Exception e) {
            log.error("elasticsearch TransportClient create error!!!", e);
        }

        return client;
    }

}
