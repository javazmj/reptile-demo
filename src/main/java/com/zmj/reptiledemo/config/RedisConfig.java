package com.zmj.reptiledemo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.redis") //redis打头的都读取出来
@Data
public class RedisConfig {

    private String host;
    private int port;
    private int timeout;
    private String password;

    @Value("${spring.redis.jedis.pool.max-active}")
    private int poolMaxActive;

    @Value("${spring.redis.jedis.pool.max-idle}")
    private int poolMaxIdle;

    @Value("${spring.redis.jedis.pool.min-idle}")
    private int poolMinIdle;

    @Value("${spring.redis.jedis.pool.max-wait}")
    private int maxWait;

}
