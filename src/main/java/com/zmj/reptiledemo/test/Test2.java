package com.zmj.reptiledemo.test;

import com.zmj.reptiledemo.utils.http.HttpClientUtils;
import org.jsoup.nodes.Document;

/**
 * 测试获取小说正文
 */
public class Test2 {

    public static void main(String[] args) throws Exception {
        String url  ="https://www.biqudu.net/43_43821/2520338.html";

        Document document = HttpClientUtils.reptile(url);

        String preHref = document.getElementsByAttributeValue("class", "pre").get(0).attr("href");
        String nextHref = document.getElementsByAttributeValue("class", "next").get(0).attr("href");
        String content = document.getElementById("content").toString();

        System.out.println(preHref);
        System.out.println(nextHref);
        System.out.println(content);
    }
}
