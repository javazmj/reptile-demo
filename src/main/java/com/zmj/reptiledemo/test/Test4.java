package com.zmj.reptiledemo.test;

/**
 * 测试获取小说目录
 */
public class Test4 {

    public static void main(String[] args) throws Exception {
        //http://www.xbiquge.la


        //https://www.qu.la


        //https://www.biquge5.com

        //https://www.xbiquge6.com

        //http://www.xbiqugew.com/

        //https://www.biqugex.com/

//=============

        /**
         * 以下所有的链接都是一本小说[ 元尊] 和 [第一章 蟒雀吞龙]  的链接
         * 小说id 和 章节id 都不一样,说明不同数据源采集后生成了自己的id和章节
         * 需要自定义
         * 要实现不同源采集很简单 入库时加上数据源code码 更新时对应从该网站更新
         * 但要实现不同源的更新比较麻烦,只能根据章节名去判断了
         */
        //https://www.xbiquge6.com/78_78513/
        //https://www.xbiquge6.com/78_78513/109585.html

        //https://www.qu.la/book/3137/
        //https://www.qu.la/book/3137/10542714.html

        //https://www.biquge5.com/0_140/
        //https://www.biquge5.com/0_140/98605.html

        //https://www.biqugex.com/book_139/
        //https://www.biqugex.com/book_139/24806185.html

        //https://www.biqudu.net/31_31729/
        //https://www.biqudu.net/31_31729/2212637.html

    }
}
