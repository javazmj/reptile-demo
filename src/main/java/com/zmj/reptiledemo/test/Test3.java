package com.zmj.reptiledemo.test;

import com.google.common.collect.Lists;
import com.zmj.reptiledemo.constant.BaseLinks;
import com.zmj.reptiledemo.repository.Book;
import com.zmj.reptiledemo.repository.BookInfo;
import com.zmj.reptiledemo.repository.BookList;
import com.zmj.reptiledemo.document.Doc2Bean;
import com.zmj.reptiledemo.utils.http.HttpClientUtils;
import com.zmj.reptiledemo.utils.http.UrlUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 测试搜索
 */
public class Test3 {

    public static void main(String[] args) throws Exception {
        //搜索不存在的小说
//        String url  = BaseLinks.searchUrl + "adsadadasdasda";

        //搜索单个小说
        String url  = BaseLinks.searchUrl + "幻想农场";

        //搜索多个小说
//        String url  = BaseLinks.searchUrl + "烟雨江南";
        Document search = HttpClientUtils.reptile(url);
        System.out.println(search);
        Elements select = search.getElementsByAttributeValue("class","item");
        if (select != null && select.size() > 0) {
            for (Element e: select) {
                Elements iamge = e.getElementsByAttributeValue("class","image");
                String id = iamge.select("a[href]").get(0).attr("href");
                String bookName = iamge.select("img").attr("alt");

//                System.out.println("name: " + bookName + " ,id : " + id);

                Document document = HttpClientUtils.reptile(BaseLinks.url + id);

                Book book = new Book();
                String trimId = UrlUtils.getTrimId(id);
                book.setId(trimId);
                Doc2Bean.Doc2BeanBook(document,book);
                //获取小说名
//            String book_name = book.getElementsByAttributeValue("property", "og:novel:book_name").
//                    get(0).attr("content");

                List<BookList> list = new ArrayList<>();

                Doc2Bean.Doc2BeanBookList(document,list,trimId,new HashSet<String>());

                List<BookInfo> infoList = Lists.newArrayList();
                for (BookList bookList:list) {
                    Document info = HttpClientUtils.reptile(BaseLinks.url + bookList.getReadUrl());
                    BookInfo bookInfo = new BookInfo();
                    Doc2Bean.Doc2BeanBookInfo(info,bookInfo);
                    bookInfo.setBookId(id);
                    bookInfo.setReadUrl(bookList.getReadUrl());
                    infoList.add(bookInfo);
                }


                Document info = HttpClientUtils.reptile(BaseLinks.url + list.get(0).getReadUrl());
                BookInfo bookInfo = new BookInfo();
                Doc2Bean.Doc2BeanBookInfo(info,bookInfo);
                bookInfo.setBookId(id);
                bookInfo.setReadUrl(list.get(0).getReadUrl());
                infoList.add(bookInfo);


                System.out.println(book.toString());
                System.out.println(list.size());
                System.out.println(bookInfo.toString());
                System.out.println(infoList.size());

            }
        }
    }
}
