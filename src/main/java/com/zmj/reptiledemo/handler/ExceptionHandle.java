package com.zmj.reptiledemo.handler;

import com.zmj.reptiledemo.exception.AuthorizeException;
import com.zmj.reptiledemo.exception.RequestLimitException;
import com.zmj.reptiledemo.exception.ResponseException;
import com.zmj.reptiledemo.utils.ResultVOUtil;
import com.zmj.reptiledemo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
@Slf4j
public class ExceptionHandle {

    /**
     *  校验错误拦截处理
     *
     * @param e 错误信息集合
     * @return 错误信息
     */
    @ExceptionHandler(BindException.class)
    public String validationBodyException(BindException e){
        log.error(e.getCause().getLocalizedMessage());
        return "/error/500";
    }

    /**
     * 参数类型转换错误
     *
     * @param e 错误
     * @return 错误信息
     */
    @ExceptionHandler(HttpMessageConversionException.class)
    public String parameterTypeException(HttpMessageConversionException e){
        log.error(e.getCause().getLocalizedMessage());
        return "redirect:/login/500";

    }

    @ExceptionHandler(value = NotFoundException.class)
//    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handlerNotFoundException(Exception e) {
        log.error(e.getCause().getLocalizedMessage());
        return "redirect:/error/404";
    }


    @ExceptionHandler(AuthorizeException.class)
    public ModelAndView handlerauthorizeException(AuthorizeException e){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/views/login");
        return modelAndView;
    }

    @ExceptionHandler(ResponseException.class)
    @ResponseBody
    public ResultVO responseException(ResponseException e){
        return ResultVOUtil.error(e.getMessage());
    }


    @ExceptionHandler(RequestLimitException.class)
    public ModelAndView requestLimitException(RequestLimitException e){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.getModel().put("msg",e.getMessage());
        modelAndView.setViewName("/error/501");
        return modelAndView;
    }

}
