package com.zmj.reptiledemo.mapper;


import com.zmj.reptiledemo.repository.ApiLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ApiLogRepository extends ElasticsearchRepository<ApiLog,String> {

}
