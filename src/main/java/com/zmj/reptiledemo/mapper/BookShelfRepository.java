package com.zmj.reptiledemo.mapper;


import com.zmj.reptiledemo.repository.BookShelf;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookShelfRepository extends ElasticsearchRepository<BookShelf,String> {

}
