package com.zmj.reptiledemo.mapper;

import com.zmj.reptiledemo.repository.BookList;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookListRepository extends ElasticsearchRepository<BookList,String>  {

}
