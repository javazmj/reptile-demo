package com.zmj.reptiledemo.mapper;


import com.zmj.reptiledemo.repository.BookUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookUserRepository extends ElasticsearchRepository<BookUser,String> {

}
