package com.zmj.reptiledemo.mapper;

import com.zmj.reptiledemo.repository.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookRepository extends ElasticsearchRepository<Book,String> {

}
