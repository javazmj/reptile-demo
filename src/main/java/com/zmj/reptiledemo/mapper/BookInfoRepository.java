package com.zmj.reptiledemo.mapper;

import com.zmj.reptiledemo.repository.BookInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BookInfoRepository extends ElasticsearchRepository<BookInfo,String> {

}
