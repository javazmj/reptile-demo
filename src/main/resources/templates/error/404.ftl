<#include "../common/header.ftl"/>
<meta http-equiv="refresh" content="3;URL=/">
<style xmlns="http://www.w3.org/1999/html">
    body{
        background: #FAFAF3;
        padding: 20px;
    }
    .row pull-left{
        background-color: #F0F0E3;
    }
    .row banner {
        padding-left: 0px;
    }
    .col-xs-12{
        padding-left: 0px;
    }
    .novellist {
        margin-top: 10px;
        margin-left: 0px;
    }
    .form-control-but {
        width: 239px;
        display: inline;
    }
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 ">
            <h1> zmj小说站</h1>
        </div>
    </div>
    <div class="row">
        <div class="form-inline">
            <input name="search" type="text" class="form-control form-control-but" placeholder="输入书名或者作者"/>
            <button type="button" class="btn btn-primary">搜索</button>
        </div>
    </div>
    <#include "../common/banner.ftl" />
    <div class="row novellist  pull-left">
        <div class="col-xs-12" style="height: 300px;text-align: center">
            <a href="/">您访问的小说不存在.3秒后跳转到首页>></a>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
<#include "../common/foot.ftl"/>
</html>