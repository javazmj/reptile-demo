<div class="row">
    <div class="col-xs-12 col-sm-12" style="text-align: left;margin: 20px 0;font-size: 16px;font-weight: bold ">
       <span> ${book.bookName}</span> <a style="color: #0066CC;" onclick="addShelf('${book.id}');">加入书架</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12" style="text-align: center;margin-bottom: 10px;">
        ${bookList.name}
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <p style="text-align: center;"><a href="${(bookInfo.pre)!}" >上一章</a><a href="/${(book.id)!}" style="margin: 0px 20px 0px 20px">目录</a><a href="${(bookInfo.next)!}" >下一章</a></p>
        <p contenteditable="true"> ${(bookInfo.content)!"无正文内容,请稍后..."}</p>
        <p style="text-align: center;"><a href="${(bookInfo.pre)!}" >上一章</a><a href="/${(book.id)!}" style="margin: 0px 20px 0px 20px">目录</a><a href="${(bookInfo.next)!}" >下一章</a></p>

    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">


    function addShelf(bookId) {
        $.ajax({
            type: "POST",
            url: "/login/addShelf",
            data: { bookId: bookId},
            dataType: "json",
            success: function(data){
                if (data.code == 0) {
                    showMessage(data.msg,1);
                    /*  window.setTimeout(function () {
                          window.location.href="/login/my";
                      }, 2000);*/
                }else {
                    showMessage(data.msg,0);
                    window.setTimeout(function () {
                        window.location.href="/login";
                    }, 2000);

                }
            }
        });
    }

    /**
     * 弹出消息提示框，采用浏览器布局，位于整个页面中央，默认显示3秒
     * 后面的消息会覆盖原来的消息
     * @param message：待显示的消息
     * @param type：消息类型，0：错误消息，1：成功消息
     */
    function showMessage(message, type) {
        let messageJQ = $("<div class='showMessage'>" + message + "</div>");
        if (type == 0) {
            messageJQ.addClass("showMessageError");
        } else if (type == 1) {
            messageJQ.addClass("showMessageSuccess");
        }
        /**先将原始隐藏，然后添加到页面，最后以600秒的速度下拉显示出来*/
        messageJQ.hide().appendTo("body").slideDown(600);
        /**3秒之后自动删除生成的元素*/
        window.setTimeout(function () {
            messageJQ.remove();
        }, 3000);
    }
</script>