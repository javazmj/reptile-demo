<div class="row">
    <div class="col-xs-12 col-sm-12">
        <#if page.prePage?? >
            <label onclick="goPage(${page.prePage});" >上一页</label>
        </#if >
        共<label style="color: red">${(page.page)!}/${(page.totalPages)!}</label>页
        去<input type="number" id="page" style="width: 26px;"/>页<button onclick="
                    inputgoPage();">Go</button>
        <#if page.nextPage??>
            <label onclick="goPage(${page.nextPage});" >下一页</label>
        </#if >
    </div>
</div>