<#include "../common/header.ftl"/>
<title>我的书架</title>
<style xmlns="http://www.w3.org/1999/html">
    body{
        background: #FAFAF3;
        padding: 20px;
    }
    .row pull-left{
        background-color: #F0F0E3;
    }
    .row banner {
        padding-left: 0px;
    }
    .col-xs-12{
        padding-left: 0px;
    }
    .novellist {
        margin-top: 10px;
        margin-left: 0px;
    }
    .form-control-but {
        width: 239px;
        display: inline;
    }
    .showMessage {
        padding: 5px 10px;
        border-radius: 5px;
        position: fixed;
        top: 45%;
        left: 45%;
        color: #ffffff;
    }
    .showMessageSuccess {
        background-color: #00B7EE;
    }

    .showMessageError {
        background-color: #ff0000;
    }
</style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-xs-12 col-sm-12  ">
            <h1>zmj小说站</h1>
        </div>
    </div>
    <#include "../common/banner.ftl" />
    <div class="row">
        <div style="margin-top: 10px;" class="col-xs-12 col-sm-12">
            ${email} 的书架 :
        </div>
        <div class="col-xs-12 col-sm-12 ">
                <hr style="height:1px;border:none;border-top:1px dashed #0066CC;width: 100%" />
                <table style="font-size: 16px;width: 100%">
                    <tbody>
                    <#if list?? && (list?size > 0)>
                    <#list list as book>
                        <tr>
                            <td >
                                <#if book.readUrl??>
                                    <a href="/${book.bookId}/${book.readUrl}">${book.bookName} / ${book.author}</a>
                                <#else>
                                    <a href="/${book.bookId}">${book.bookName} / ${book.author}</a>
                                </#if>
                            </td>
                            <td>
                                <a style="color: #0066CC;" onclick="underShelf('${book.bookId}');">下架</a></br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr style="height:1px;border:none;border-top:1px dashed #0066CC;" />
                            </td>
                        </tr>
                    </#list>
                    </tbody>
                </table>
                <#else>
                    <div style="font-size: 16px;text-align: center;color: red" > ${msg!}</div>
                    <hr style="height:1px;border:none;border-top:1px dashed #0066CC;" />
                </#if>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<#--<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">

    function underShelf(bookId) {
        $.ajax({
            type: "POST",
            url: "/login/under",
            data: { bookId: bookId},
            dataType: "json",
            success: function(data){
                if (data.code == 0) {
                    showMessage(data.msg,1);
                    window.setTimeout(function () {
                        window.location.href="/login/my";
                    }, 2000);

                }
            }
        });
    }

    /**
     * 弹出消息提示框，采用浏览器布局，位于整个页面中央，默认显示3秒
     * 后面的消息会覆盖原来的消息
     * @param message：待显示的消息
     * @param type：消息类型，0：错误消息，1：成功消息
     */
    function showMessage(message, type) {
        let messageJQ = $("<div class='showMessage'>" + message + "</div>");
        if (type == 0) {
            messageJQ.addClass("showMessageError");
        } else if (type == 1) {
            messageJQ.addClass("showMessageSuccess");
        }
        /**先将原始隐藏，然后添加到页面，最后以600秒的速度下拉显示出来*/
        messageJQ.hide().appendTo("body").slideDown(600);
        /**3秒之后自动删除生成的元素*/
        window.setTimeout(function () {
            messageJQ.remove();
        }, 3000);
    }
</script>
</body>
<#include "../common/foot.ftl"/>
</html>