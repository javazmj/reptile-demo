<#include "../common/header.ftl"/>
<title>zmj小说站注册</title>
<style xmlns="http://www.w3.org/1999/html">
    body{
        background: #FAFAF3;
        padding: 20px;
    }
    .row pull-left{
        background-color: #F0F0E3;
    }
    .row banner {
        padding-left: 0px;
    }
    .col-xs-12{
        padding-left: 0px;
    }
    .novellist {
        margin-top: 10px;
        margin-left: 0px;
    }
    .form-control-but {
        width: 239px;
        display: inline;
    }
    .showMessage {
        padding: 5px 10px;
        border-radius: 5px;
        position: fixed;
        top: 45%;
        left: 45%;
        color: #ffffff;
    }
    .showMessageSuccess {
        background-color: #00B7EE;
    }

    .showMessageError {
        background-color: #ff0000;
    }
</style>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 ">
            <h1> zmj小说站</h1>
        </div>
    </div>
    <#include "../common/banner.ftl" />
    <div class="row novellist  pull-left" >
        <div class="display-4" style="margin-bottom: 30px;margin-top: 30px;">
            <form>
                <p>
                    <label style="font-size: 18px">邮箱:</label>
                    <input style="margin-top: 10px;" type="email" id="email" name="email" class="form-control form-control-but"  />
                </p>
                <p>
                    <label style="font-size: 18px">密码:</label>
                    <input style="margin-top: 10px" type="password" id="password" placeholder="密码长度为6~16位之间" name="password" minlength="6" maxlength="16" class="form-control form-control-but"/>
                </p>
                <p>
                    <label style="font-size: 18px">验证码:
                    </label><input style="margin-top: 10px;width: 100px;" type="number" id="verifCode" name="verifCode" minlength="6" maxlength="6" class="form-control form-control-but"/>
                    <button style="margin-top: 10px;" type="button" class="btn btn-primary" id="send"">获取</button>
                </p>
                <p>
                    <button style="margin-top: 10px;" type="button" id="register" class="btn btn-primary">注册</button>
                </p>

            </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<#--<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">

    var checkEmail = false;

    $("#email").blur(function(){
        var email = $("#email").val();
        $.ajax({
            type: "GET",
            url: "/login/checkEmail",
            data: { email: email},
            dataType: "json",
            success: function(data){
                if (data.code == 0) {
                    checkEmail = false;
                } else {
                    showMessage(data.msg,0);
                    $("#emial").empty();
                    $("#emial").focus();
                    checkEmail = true;
                }

            }
        });
    })

    $('#send').click(function(){

        var email = $("#email").val();
        //对电子邮件的验证
        var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if(!myreg.test(email)){
            showMessage("电子邮箱格式不正确",0);
            myreg.focus();
            return false;
        }

        if (checkEmail == true) {
            showMessage("该邮箱已注册",0);
            return false;
        }

          $.ajax({
              type: "GET",
              url: "/login/registerSend",
              data: { email: email},
              dataType: "json",
              success: function(data){
                  if (data.code == 0) {
                      showMessage("验证码已发送至您的邮箱,请注意查收",1);
                  } else {
                      showMessage(data.msg,0);
                      $("#emial").empty();
                      $("#emial").focus();
                  }

              }
          });
    });

    $('#register').click(function(){

        var email = $("#email").val();
        var password = $("#password").val();
        var verifCode = $("#verifCode").val();


        if (checkEmail == true) {
            showMessage("该邮箱已注册",0);
            return false;
        }

        if (email == null || email == "") {
            showMessage("邮箱不填你确定要注册?",0);
            return false;
        }
        if (password == null || password == "") {
            showMessage("注册账号密码都不要,有趣的人,我记住你了!",0);
            return false;
        }
        if (password.length < 6 || password.length > 16) {
            showMessage("太弱的密码了,简直弱鸡啊~~",0);
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/login/register",
            data: { email: email,password: password,verifCode: verifCode },
            dataType: "json",
            success: function(data){
                if (data.code == 0) {
                    showMessage(data.msg,1);
                    window.location.href = "/login/my"
                } else {
                    showMessage(data.msg,0);
                    $("#email").empty();
                }

            }
        });
    });
    /**
     * 弹出消息提示框，采用浏览器布局，位于整个页面中央，默认显示3秒
     * 后面的消息会覆盖原来的消息
     * @param message：待显示的消息
     * @param type：消息类型，0：错误消息，1：成功消息
     */
    function showMessage(message, type) {
        let messageJQ = $("<div class='showMessage'>" + message + "</div>");
        if (type == 0) {
            messageJQ.addClass("showMessageError");
        } else if (type == 1) {
            messageJQ.addClass("showMessageSuccess");
        }
        /**先将原始隐藏，然后添加到页面，最后以600秒的速度下拉显示出来*/
        messageJQ.hide().appendTo("body").slideDown(600);
        /**3秒之后自动删除生成的元素*/
        window.setTimeout(function () {
            messageJQ.remove();
        }, 3000);
    }
</script>
</body>
<#include "../common/foot.ftl"/>
</html>