<#include "../common/bookheader.ftl"/>
<title>${book.bookName}</title>
</head>
<body>
<div class="row">
    <div class="col-xs-12 col-sm-12  ">
        <h1> zmj小说站</h1>
    </div>
</div>
<#include "../common/banner.ftl" />
<div class="row">
    <input type="hidden" id="bookId" value="${book.id}" />
    <input type="hidden" id="sort" value="${(sort)!"ASC"}" />
    <input type="hidden" id="totalPages" value="${page.totalPages}" />

    <div class="col-xs-12 col-sm-12" style="display: flex; justify-content: space-between;">
        <table>
            <tbody>
            <tr>
                <td colspan="2"><img src="${book.image}" width="80px" height="80px"></td>
                <td>
                    ${book.category}</br>
                    <span style="color: red;">${book.bookName} / ${book.author}</span></br>
                    ${book.status}</br>
                    <a style="color: #0066CC;" onclick="addShelf();">加入书架</a></br>

                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <p>小说简介:</p>
                    <p>${book.description}</p>
                    <p style="font-size: 16px">最新章节: <a href="/${(book.latestChapterUrl)!"${book.id}"}" >${book.latestChapterName} </a> </p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12" style="display: flex; justify-content: space-between;margin-bottom: 10px">
        <div >
            <span>章节目录:</span>
            <span><img property="ASC" src="/static/image/sort.png" style="width: 25px;height: 25px;" onclick="pageSort()"></span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12" style="display: flex; justify-content: space-between;">
        <ul  class="list-unstyled" >
            <#list page.items as book>
                <li><a href="${book.readUrl}" >${book.name} </a></li><br>
            </#list>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12" style="display: flex; justify-content: space-between;">
        <p>
            <#include "../common/page.ftl" />
        </p>
    </div>
</div>

<script type="text/javascript">
    document.write("<link rel='stylesheet' type='text/css' href=/static/css/index.css?random='"+Math.random()+"'></s"+"cript>");
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<#--    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">

    function addShelf(bookId) {
        var bookId = $("#bookId").val();
        $.ajax({
            type: "POST",
            url: "/login/addShelf",
            data: { bookId: bookId},
            dataType: "json",
            success: function(data){
                if (data.code == 0) {
                    showMessage(data.msg,1);
                    window.setTimeout(function () {
                        window.location.href="/login/my";
                    }, 2000);
                }
            }
        });
    }


   function pageSort() {
       var bookId = $("#bookId").val();
       var sort = $("#sort").val();
       if (sort == "ASC") {
           sort = "DESC";
       }else {
           sort = "ASC";
       }
       window.location.href="/" + bookId + "?sort=" + sort ;
   }
   function goPage(page) {
       var totalPages = $("#totalPages").val();
       if (page > totalPages) {
           showMessage("睁大眼睛看清楚多少页!",0);
           return;
       }
       if (page < 1) {
           showMessage("你是来搞笑的么?",0);
           return;
       }
       var bookId = $("#bookId").val();
       var sort = $("#sort").val();

       window.location.href="/" + bookId + "?page=" + page + "&sort=" + sort ;
   }
   function inputgoPage() {
       var page = $("#page").val();
        goPage(page)
   }
   /**
    * 弹出消息提示框，采用浏览器布局，位于整个页面中央，默认显示3秒
    * 后面的消息会覆盖原来的消息
    * @param message：待显示的消息
    * @param type：消息类型，0：错误消息，1：成功消息
    */
   function showMessage(message, type) {
       let messageJQ = $("<div class='showMessage'>" + message + "</div>");
       if (type == 0) {
           messageJQ.addClass("showMessageError");
       } else if (type == 1) {
           messageJQ.addClass("showMessageSuccess");
       }
       /**先将原始隐藏，然后添加到页面，最后以600秒的速度下拉显示出来*/
       messageJQ.hide().appendTo("body").slideDown(600);
       /**3秒之后自动删除生成的元素*/
       window.setTimeout(function () {
           messageJQ.remove();
       }, 3000);
   }
</script>
</body>
<#include "../common/foot.ftl"/>
</html>