# reptile-demo
--author: zmj

--email: 536304123@qq.com

--current version: 2.0.0-SNAPSHOT

########################################

    严正声明,本项目仅供学习使用,禁止商业用途.

########################################

#### 介绍
使用java原生Jsoup采集小说数据的demo 

演示地址 : http://book.zmjmall.com


#### 软件说明


1. 项目启动时MyCommandRunner 通过 JobThread 采集BaseLinks里的所有分类下的分类小说链接缓存到redis
    (JobThread 调用 JobBookListThread 将小说名和小说章节目录存储到数据引擎,并将采集过的小说id缓存到redis)

2. ReptileBookJob 类定时通过 JobBookListThread 采集 redis里缓存的小说和目录到数据引擎.并在redis记录已经采集过的小说id


3. ReptileBookInfoJob 类定时通过JobBookInfoThread 将已经采集过的小说里的正文采集到数据引擎.
   
    (当时小说目录的数据是 1001492 条,Mysql的话当时采集时间为4个多小时,Es的话是用了一个晚上)
   
    (此步骤是最耗时的,正文内容为text文本,必须开多线程.否则的话就喝杯茶等到老死吧.)
   
    ###(取巧方法,可以只采集小说名称内容和目录内容 正文内容可以在用户点击的时候在采集数据后生成静态页面,
    存储到redis,设置缓存失效时间,这样大大的减少了自己数据库的压力)
    
    初始化采集完毕后将 MyCommandRunner 里方法注释,已经不需要了,
    将 ReptileBookJob 的定时任务注释,已经不需要了
    将 bookInfoJob 定时任务酌情延长 ,发现站外有更新就会自动更新
    

4. 用户搜索小说后查找站内数据,如果没有则将去采集站外搜索.
    如果搜索返回null则返回给用户null
    如果搜索到数据,则将搜索到的小说id列表存入redis  search_link_urls(等待搜索采集列表) 
    
5. ReptileUpdateBookJob 类定时通过 jobBookListThread 采集小说和目录到数据引擎,完成后在redis记录已经采集过的小说id
    
    
6. ReptileUpdateBookInfoJob 类定时通过 jobBookInfoThread 采集小说正文到数据引擎.
    采集完成后删除redis  search_link_urls(等待搜索采集列表) 
    由于用户实时搜索,所以5和6的定时任务设置的间隔时间比较短. 由于网络等原因可能会出现HttpClient连接超时或者读取超时等的错误
    所以定时任务反复去多线程采集

7.  随着用户搜索的增多,去采集的数据也会越多

8.  介绍完毕.

#### 使用说明

    如果用Mysql存储将 sql 下的 novel.sql 建表
    如果用es 则搭建es,搭建过程具体不做介绍
    修改 application.yml redis mysql或者es的配置
    然后认真阅读 上面软件说明
    启动即可
  
###测试环境

阿里云CentOs  7.2 2G4核 2M带宽

ElasticSearch版本  5.5.2

Springboot版本 2.0.4.RELEASE

Mysql 5.7   

###注意事项

  用mysql的话尤其正文查询会很慢,适当的将热点数据做缓存处理
  
  用Es做数据引擎查询速度没的说,但是比较吃服务器配置,以上的服务器配置只跑了es 和 该项目就Es经常性挂掉或者项目内存溢出
  没办法只能一台服务器运行es,一台运行Web,用Nginx做转向
  
  
  
   
  
####2.1.0 更新内容 update: 219/06/25
    优化采集逻辑
    优化界面
    增加搜索后实时采集功能
    增加注册登录功能
    增加加入书架功能
    增加记住登录用户阅读章节功能
####2.0 更新内容  update:219/06/23
                 优化采集逻辑
    1.将数据存储更换为es
  
####1.0 更新内容  update:219/06/22
    首页根据分类显示
    排行榜
    小说目录
    小说正文
    
#### 计划版本2.1.1
    1.增加多数据源搜索